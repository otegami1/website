---
title: 代表取締役の須藤がApache ArrowのPMC chairに就任
author: kou
tags:
  - apache-arrow
---

[Apache Arrow](https://arrow.apache.org/)の開発に参加している代表取締役の須藤です。

<!--more-->

2016年からかれこれ6年くらい開発に参加しているApache Arrowプロジェクトなんですが、このたび私がPMC chairになりました！すごくない？

アナウンスメール：[\[ANNOUNCE\] New Arrow PMC chair: Kouhei Sutou](https://lists.apache.org/thread/zlfkst96hv32bkb4pssx67pzfcx5d142)

## PMC chair

[Apache Software Foundation](https://www.apache.org/)傘下のプロジェクトのことを知らない人からすれば「PMC chairってなに？」だと思うので、まずそのへんを説明します。

Apache Software Foundation傘下の各プロジェクトはそれぞれ[Project Management Committee（プロジェクト管理委員会）](https://www.apache.org/foundation/how-it-works.html#pmc)が管理することになっています。PMCがすることはコードを書くこと、ではありません！健全なコミュニティーを維持することです。コミュニティーが法的な問題に遭遇しても確実に解決したり、コミュニティーがあらかじめ決められた手続きを守ることだったり（たとえば、リリース時にOpenPGPキーでサインするとか）、コミュニティーが拡大できるようにしたりとかです。その名の通りプロジェクト管理です。

PMCは複数人で構成されているのでみんなでプロジェクトを管理しています。Apache ArrowのPMCのメンバーは https://arrow.apache.org/committers/ で確認できます。

で、PMC chairはなにかというとPMCの委員長です。各PMCはApache Software Foundationの理事会とやりとりする必要があるのですが、PMCを代表してそのやりとりをするのがPMC chairです。他にも新しいPMCメンバーの権限を設定するとかがありますが、基本的に書類仕事みたいなことです。プロジェクト管理自体はPMCのみんなで一緒にやるので、PMC chairがすごくがんばってプロジェクト管理をするわけではありません。

ということで、PMC chairは別にすごくないんですねぇ。すごいと思ったでしょ？残念でした。

## Apache ArrowのPMC chairの運用

どうして私がこのタイミングでPMC chairになったかについても説明します。

実は、1年ほど前からApache ArrowはPMC chairを毎年入れ替える運用にしました。

毎年PMC chairを入れ替えるのはどう？の議論：[\[DISCUSS\] Rotating the PMC Chair](https://lists.apache.org/thread/mvsnj0zqf33wztcl2pvryq3rf0l3s57y)

それを受けて、2020年10月にApache ArrowのPMC chairが2代目になりました。これまで同じ人がずっとApache ArrowのPMC chairでしたが、このタイミングで[WesさんがPMC char](https://lists.apache.org/thread/2b16pj6voxokd04tt53ww54hh84y6fqn)になりました。WesさんはApache Arrowの立ち上げ時から開発に参加している人でもあります。もしかしたら、pandasの作者として知っている人も多いかもしれません。

それから1年くらいたったので「そろそろPMC chairを入れ替えようぜー」という話になりました。その中で「次は須藤さんがいいんじゃない？」という話がでて、みんなが「いいんじゃね？」となり、私も「いいよー」と言ったので、手続きが進み始めました。

そろそろPMC chairを入れ替えようぜーの議論：[\[DISCUSS\] Annual rotation of Arrow PMC chair](https://lists.apache.org/thread/gr5krdonftbslx01qjob59dcvbnpwljx)

で、もろもろ手続きが完了して私が正式に3代目のApache Arrow PMC chairになりました。

最初に書いた通り6年くらいApache Arrowの開発に参加していて古参の方なので、いつかは回ってくるだろうなぁとは思っていましたが、もう回ってくるとは思っていなかったのでびっくりしました。作業内容的に英語を読み書きする機会が増えるのでしんどいのですが、[技術用語集を作っている吉本さん]({% post_url 2021-12-01-introducing-glossary %})が英語を助けてくれるし、せっかくの機会なので今年頑張ることにしました。

## まとめ

Apache Arrowの3代目PMC chairになったので、どういう役割なのかとどういう経緯でなったのかを説明しました。

お祝いしたい！という人は、私はApache Arrow関連の案件（Apache Arrow自体を開発してお金をもらいたい！）を探しているのでそういう情報を[お問い合わせフォーム]({% link contact/index.md %})から教えてください！

参考：[Apache Arrowのサポートサービス]({% link services/apache-arrow.md %})

今回の事例は、英語が得意でない（し好きでもない）人でも海外の重要OSSのリーダーになることができる事例といえます。OSSの開発に参加したい！そんな経験のある人にサポートして欲しい！という人は[お問い合わせフォーム]({% link contact/index.md %})からご相談ください。[SpeeeさんのOSS開発をサポート](https://tech.speee.jp/entry/2021/03/01/182057)している実績があります。

この話とは関係ないのですが、「OSS Gateオンボーディング」の一環で[MariaDBの開発に継続的に参加する人を増やす企画](https://oss-gate.github.io/on-boarding/proposals/2022-03/nayuta-mariadb-server/proposal.html)を進めています。近いうちに「OSS Gateオンボーディング」についてもこのブログで紹介できるといいな。
