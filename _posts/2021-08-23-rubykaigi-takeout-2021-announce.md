---
tags:
- ruby
- presentation
title: '事前情報：RubyKaigi Takeout 2021 - Red Arrow - Ruby and Apache Arrow - チケットプレゼントもあるよ #rubykaigi'
author: kou
---

[RubyKaigi Takeout 2021](https://rubykaigi.org/2021-takeout/)で[Red Arrow - Ruby and Apache Arrow](https://rubykaigi.org/2021-takeout/presentations/ktou.html)という[Apache Arrow](https://arrow.apache.org/)のオフィシャルRubyライブラリーの話をする須藤です。RubyKaigi Takeout 2021での私の話をより理解できるようになるために内容を紹介します。

なお、[クリアコードはゴールドスポンサーとしてRubyKaigi Takeout 2021を応援](https://rubykaigi.org/2021-takeout/sponsors#sponsor-280)しています。ゴールドスポンサーになるとチケットをもらえるのですが社内では使い切れないので欲しい人にあげます。2名分あります。応募方法はこの記事の最後を参照してください。

<!--more-->

<div class="youtube">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/okXiuYiP2C4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/rubykaigi-takeout-2021/viewer.html"
          width="640" height="404"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/rubykaigi-takeout-2021/" title="Red Arrow - Ruby and Apache Arrow">Red Arrow - Ruby and Apache Arrow</a>
  </div>
</div>

関連リンク：

  * [動画（YouTube）](https://www.youtube.com/watch?v=okXiuYiP2C4)

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/rubykaigi-takeout-2021/)

  * [スライド（SlideShare）](https://www.slideshare.net/kou/rubykaigi-takeout-2021)

  * [リポジトリー](https://gitlab.com/ktou/rabbit-slide-kou-rubykaigi-takeout-2021)

### 背景

私はRubyが好きなのでデータ処理をするときもできるだけRubyを使いたいです。が！残念ながらRubyにはデータ処理をするためのライブラリー・ツールが揃っていないため使えませんでした。そこでデータ処理をするためのライブラリー・ツールを整備していくことにしました。そのためにはじめたプロジェクトが[Red Data Tools](https://red-data-tools.github.io/ja/)です。2016年ころの話です。

ちょうどその頃[Apache Arrow](https://arrow.apache.org/)のことを知りました。Apache Arrowはいろんなプログラミング言語に対応したデータ分析ツール開発プラットフォームを目指しているのですが、「いろいろなプログラミング言語に対応した」という部分に惹かれました。この人たちと一緒に開発すればRuby用のデータ処理ツールを全部Rubyの人たちだけで整備しなくてもいいじゃん！ということで、私はApache Arrowの開発に参加し始めました。主にRuby実装の作業をしていますが、Ruby実装はC++実装に依存しているためC++実装もちょいちょい開発しています。

ときは流れて去年、Apache Arrow 1.0.0がリリースされました。初のメジャーリリースです。RubyKaigi 2020のCFPの締め切り後だったのでRubyKaigi 2020は違う話題（[Goodbye fat gem]({% post_url 2020-09-03-index %})）で応募しました。

そして今年、Apache Arrowがメジャーリリースされて初のRubyKaigi（Takeout）のCFPの募集がはじまりました。ついに私のこの5年の成果をRubyistに紹介する機会がきたのです！

### 内容

トークの時間は25分なので、網羅的に紹介しつつ詳細も紹介するということはできません。個別の詳細は[db tech showcase ONLINE 2020で話し]({% post_url 2020-12-07-index %})たりしていたので、網羅的に紹介することにしました。RubyKaigiで他で話したような内容を使いまわしたくはなかったですし。

網羅的な紹介にしたので詳細が気になる部分が出てくるとは思います。詳細の参考URLを入れているので、気になる人はそれを見たり、RubyKaigi Takeout 2021のトークの時間にYouTubeのコメントで私に聞いたり、Twitterで[@ktou](https://twitter.com/ktou)に聞いたり、[Red Data Toolsのチャット](https://gitter.im/red-data-tools/ja)で私に聞いたりしてください。

資料・トークの中ではちょいちょい動くコードを紹介していますが、実はまだ私の手元でしか動かないコードも混ざっています。まだmasterにマージされていない実装があるからなのですが、トークのときまでにはすべてmasterに入っているはずです。（トークの事前収録は今日提出したのでRubyKaigi Takeout 2021のトークは今日時点の情報で作られているのです。）資料はすでに公開しているので資料の中のコードは試せますが、まだ動かないものがあることに注意してください。

短く速く動くコードを選んだつもりなのでワクワクしてもらえるとうれしいな。以下は日本の郵便番号データから東京都のデータだけに絞り込むコードです。絞り込み処理はApache ArrowのC++実装で動くので高速です。

```ruby
require "datasets-arrow"
table = Datasets::PostalCodeJapan.new.to_arrow
table.n_rows # 124271
filtered_table = table.slice do |slicer|
  slicer.prefecture == "東京都" # Tokyo
end # 0.001s
filtered_table.n_rows # 3887
```

以下のグループ化して集計する処理もApache ArrowのC++実装で動くので高速です。

```ruby
table.group(:prefecture).count(:postal_code)
# #<Arrow::Table:0x7fcaf46f3a58 ptr=0x565135759770>
# 	count(postal_code)	prefecture
#  0	              8248	北海道       
#  1	              2516	青森県       
#  2	              2074	岩手県       
#  3	              3382	宮城県       
#  4	              2158	秋田県       
#  5	              1958	山形県       
#  6	              3950	福島県       
#  7	              2858	茨城県       
#  8	              1834	栃木県       
#  9	              1501	群馬県       
# ...
# 37	              1750	愛媛県       
# 38	              1695	高知県       
# 39	              3294	福岡県       
# 40	               872	佐賀県       
# 41	              1894	長崎県       
# 42	              1897	熊本県       
# 43	              1849	大分県       
# 44	               877	宮崎県       
# 45	              1459	鹿児島県      
# 46	               800	沖縄県       
```

ワクワクして自分もRuby用のデータ処理ツールを整備していきたい！と思った人は[Red Data Tools](https://red-data-tools.github.io/ja/)の[チャット](https://gitter.im/red-data-tools/ja)に来てください。一緒に整備していきましょう！そういう人が増えたら私の今回のトークは成功です。

Red Data Toolsはいつも新しく参加してくる人を受け入れているのですが、それをもう少し一般化してOSSプロジェクトに新しく参加してくる人の受け入れをサポートする取り組みを始めています。[OSS Gateオンボーディング](https://oss-gate.github.io/on-boarding/)という取り組みです。OSSの開発に継続的に参加する人を増やしたいのです。「自分が関わっているOSSプロジェクトにもっと参加して欲しい！」という人はTwitterで[@ktou](https://twitter.com/ktou)に連絡をください。力になれるはずです。

### Rabbit

今回の資料もいつものようにRubyist用のプレゼンツール[Rabbit](https://rabbit-shocker.org/)で作りました。Rabbitで資料を作るとRabbitの開発が進むのですが、今回もついついRabbitをいじってしまいました。

私は事前収録の動画はRabbitのウィンドウを[OBS Studio](https://obsproject.com/ja)で取り込んで作っているのですが、そうするとターミナルを使ったデモができません。Rabbitにターミナルが組み込まれていなくてRabbitとは別のウィンドウを使わなくてはいけないからです。別撮りにして後で編集してつなげればいいのですが、Rabbitにターミナルを組み込みました。組み込んだほうがかっこいいし！これでRabbitのウィンドウだけ取り込んでいてもターミナルでデモできます。

Rabbitは[GTK](https://www.gtk.org/)を使っているのでGTK用のターミナルウィジェットである[VTE](https://gitlab.gnome.org/GNOME/vte)を使っています。masterにはSIXELサポートが入っていたのでデモで使おうと思ったのですが見せ場を用意できませんでした。残念。

Apache Arrowの話をすると速さの話をすることが多くなります。大規模データの処理には速さが重要だからです。Red Arrowの話をするときもやはり速さの話がでてくるので、今回の資料でもいくつかベンチマーク結果を載せています。ベンチマーク結果は数値だけよりもグラフにした方がわかりやすいのでできるだけグラフを作るのですが、今回はRabbitに[Charty](https://github.com/red-data-tools/charty/)を組み込んでRabbit内で実行時にグラフを作って描画するようにしました。Rabbitがまたかっこよくなってしまった。

ChartyはRed Data Toolsプロジェクトで開発している可視化ライブラリーです。今回の資料でも紹介していますし、RubyKaigi Takeout 2021にも[Charty: Statistical data visualization in Ruby](https://rubykaigi.org/2021-takeout/presentations/mrkn.html)というトークがあります。Rubyで可視化したい人はチェックしてください。

### チケットプレゼント

去年も[スポンサーとしてRubyKaigi Takeoutを応援]({% post_url 2020-09-03-index %})しましたが、今年も[スポンサーとしてRubyKaigi Takeoutを応援](https://rubykaigi.org/2021-takeout/sponsors#sponsor-280)します。今回はチケットをもらったのですが、クリアコード関係者だけでは使い切れないのでRubyKaigi Takeout 2021に参加したい人にプレゼントします。詳細は次の通りです。

  * 定員：2名
  * 応募資格：
    * 2021-09-09（木）-2021-09-11（土）に行われるRubyKaigi Takeout 2021に参加できる人
    * Twitterの[@_clear_code](http://twitter.com/_clear_code/)からのダイレクトメッセージで当選の通知を受け取れる方
  * 応募方法：
    1. Twitterで[@_clear_code](http://twitter.com/_clear_code/)をフォロー
    2. 2021-09-01（水）00:00:00 JSTまでにTwitterで[@_clear_code](http://twitter.com/_clear_code/)へ「#rubykaigi RubyKaigi Takeout 2021に参加したい！ {{ page.url | absolute_url }} 」からはじめて思いの丈を綴った上でツイートしてください。例：「@_clear_code #rubykaigi RubyKaigi Takeout 2021に参加したい！ {{ page.url | absolute_url }} Rubyでデータ処理したい！！！」
  * 選考：
    * 当選者は[@_clear_code](http://twitter.com/_clear_code/)およびこのブログで発表
    * 2021-09-01（水）に当選者にTwitterの[@_clear_code](http://twitter.com/_clear_code/)からダイレクトメッセージで詳細を通知
    * 自分のブログなどにRubyKaigi Takeout 2021の参加レポートを書いてくれる方を優先
    * 最近、[採用ページ]({% link recruitment/index.md %})を更新したので今の採用ページを読んでみた感想を教えてくれる方を優先

### まとめ

RubyKaigi Takeout 2021でRed Arrowの話をします。[Red Data Tools](https://red-data-tools.github.io/ja/)の仲間が増えるといいな。[OSS Gateオンボーディング](https://oss-gate.github.io/on-boarding/)に興味がある人がいるといいな。

RubyKaigi Takeout 2021をスポンサーとして応援します。

スポンサーでもらったチケットが余るので欲しい人にプレゼントします。
