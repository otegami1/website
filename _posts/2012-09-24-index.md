---
tags:
- ruby
title: "「札幌Ruby会議2012のチケットを譲るキャンペーン」当選者のレポート紹介"
---
[札幌Ruby会議2012のチケットを譲るキャンペーン]({% post_url 2012-06-21-index %})に[当選したみなさん]({% post_url 2012-08-06-index %})のレポートを紹介します。
<!--more-->


### [@hayabusa333](https://twitter.com/hayabusa333)さん

[ハードリカーエンジニア　 札幌Ruby会議2012に行ってきました #sprk2012](http://hayabusa39.blog99.fc2.com//blog-entry-115.html)

都合が悪く初日は参加できなかったとのことですが、いろいろ得たものがあったようです。よかったです。

<blockquote>

このご恩はコミュニティーへの貢献と良いコードでのお返しをしていきたいと思います。

先ずは東京Ruby会議10のLTに応募します。

</blockquote>

実際に行動を始めていてすばらしいです。

### [@vestige_](https://twitter.com/vestige_)さん

[札幌Ruby会議2012にいってきた！ - Vestige is fragments of memory](http://d.hatena.ne.jp/vestige/20120925)

<blockquote>

須藤さんの発表は何度か聞いてますが、毎回、なにかやらなきゃって気持ちにさせてくれる。

</blockquote>

ぜひ、なにかやりはじめてください！

### [@Satoko_R](https://twitter.com/Satoko_R/)さん

[Ruby会議に行ってきました | さとこぶろぐ](http://satoko.logon-hakodate.net/?p=28)

<blockquote>

わからないと言っているといつまでもわからないままなので少しでもコードを書こうと思い、dRubyやSmalltalkのハンズオンに参加しました。また、Lindaさんやyokoさん、万葉の方々にRails Girlsについてのお話を伺ったりして、自分なりに色々なことを吸収できたように思います。

</blockquote>

ほとんどコードを書いたことがないというさとこさんですが、コードを書くハンズオンに積極的に参加していたそうです。会期中から積極的に行動し始めていてすばらしいです。

<blockquote>

最後の講演の後、ClearCodeの 須藤さんとお話ししたときに、

「コードを書いたことない私がチケット頂いちゃってよかったんですか?」

と言ったら

「コードで返してくれればいいです!」

と言われ、すごく嬉しかったと同時に身が引き締まる思いでした。

</blockquote>

楽しみにしています！

### まとめ

みなさん、札幌Ruby会議2012で何かを得て、そして実際に行動を始めていたようです。キャンペーンを実施してよかったです。
