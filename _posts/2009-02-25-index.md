---
tags: []
title: ActiveLdap 1.0.2リリース
---
先日、ActiveLdap 1.0.2がリリースされました。（[リリースアナウンス](http://rubyforge.org/pipermail/ruby-activeldap-discuss-ja/2009-February/000001.html:)）
<!--more-->


インストール:

{% raw %}
```
% sudo gem install activeldap
```
{% endraw %}

ActiveLdapはLDAPの各エントリを通常のオブジェクトのように便利に扱うためのRuby用のライブラリです。リレーショナルデータベースのレコードを便利に扱うためのActiveRecordのLDAPバージョンというとイメージしやすいかもしれません。

### Net::LDAPとのAPIの比較

Rubyで実装されたLDAPクライアントのNet::LDAPとActiveLdapのAPIを比べてみます。まず、LDAPサーバにTLSで接続し、バインドします。その後、エントリを検索し、属性を変更します。

Net::LDAP:

{% raw %}
```ruby
require 'net/ldap'

Net::LDAP.open(:host => LDAP_HOST,
               :port => 389,
               :encryption => :start_tls,
               :auth => {
                 :method => :simple,
                 :username => LDAP_USER,
                 :password => LDAP_PASSWORD
               }) do |ldap|
  entry = ldap.search(:base => ou=People,#{LDAP_BASE}",
                      :scope => Net::LDAP::SearchScope_WholeSubtree,
                      :filter => "(uid=*)")[0]
  ldap.modify(:dn => entry.dn,
              :operations => [[:replace, "sn", "new-sn"]])
end
```
{% endraw %}

ActiveLdap:

{% raw %}
```ruby
require 'active_ldap'

ActiveLdap::Base.establish_connection(:host => LDAP_HOST,
                                      :base => LDAP_BASE,
                                      :method => :tls,
                                      :bind_dn => LDAP_USER,
                                      :password => LDAP_PASSWORD)
class LdapUser < ActiveLdap::Base
  ldap_mapping :dn_attribute => "uid",
               :prefix => 'ou=People',
               :classes => ["person"]
end

entry = LdapUser.find(:first)
entry.sn = "new-sn"
entry.save
```
{% endraw %}

Net::LDAPでは主要なオブジェクトがLDAPクライアントを表しているNet::LDAPオブジェクトなのに対して、ActiveLdapではエントリを表しているLdapUserオブジェクトになっています。そのため、接続・検索するだけであればNet::LDAPの方がActiveLdapよりもシンプルです。クラスを定義する必要もありません。しかし、エントリを変更する必要がある場合はActiveLdapの方がシンプルです。

Net::LDAPとActiveLdapのAPIは対象としている利用者が違います。状況に応じて使い分けて下さい。

  * 接続・検索するだけ: Net::LDAP
  * エントリも操作したい: ActiveLdap

アプリケーションではNet::LDAPを使い、自動化されたテスト用のデータ作成・削除にActiveLdapを使う、ということも考えられます。

Net::LDAPとActiveLdapの速度の違いについては以前触れました: [ActiveLdap 1.0.1リリース]({% post_url 2008-06-15-index %})

### 1.0.1 -> 1.0.2

1.0.2ではRails 2.2.2に対応しています。ただし、国際化まわりについては対応を見送っています。

また、ActiveDirectoryへの対応も強化されました。これは、クリアコードの業務でActiveDirectoryのエントリを操作するためにActiveLdapを利用したことの影響でもあります。

他にも、10以上のバグが修正されています。1.0.1を利用している場合は1.0.2へのアップグレードをおすすめします。

### まとめ

RubyでLDAPエントリを操作したい場合はActiveLdapが便利です。ActiveLdapは継続的に開発が続いていて、安心して使えるフリーソフトウェアです。

問題・要望がある場合は[メーリングリスト](http://rubyforge.org/mailman/listinfo/ruby-activeldap-discuss-ja)などでどうぞ。
