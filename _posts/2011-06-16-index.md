---
tags:
- ruby
title: 日本Ruby会議2011でLDAPの話をしませんか？
---
[日本Ruby会議2011](http://rubykaigi.org/2011/ja)の[Lightning Talkの締め切り](http://rubykaigi.tdiary.net/20110527.html#p01)が残り数日になっていますが、みなさんいかがお過ごしでしょうか。
<!--more-->


去年開催された[日本Ruby2010](http://rubykaigi.org/2010/ja)では、は[Ruby で扱う LDAP のススメ - 選択肢とその事例](http://rubykaigi.org/2010/ja/events/99)という企画があり、RubyとLDAPに関する情報を共有する場になりました。（[企画を主催した高瀬さんのまとめ](http://d.hatena.ne.jp/tashen/20100901/1283365699)）

日本では「RubyでLDAPを使う」というケースを情報共有する場が無く、この企画はとても意義のあるものでした。今年も同じような場が設けられると有意義でしょう。ということで、[RubyKaigi2011の講演以外の企画について](http://rubykaigi.tdiary.net/20110514.html)を参考に、日本
Ruby会議2011でそういう場を設けるための方法案をまとめてみました。興味がでてきた人はぜひ実現させてください。

### Lightning Talk

自分が知っていることなどを発表して情報を共有できるでしょう。ただし、持ち時間5分のなかで質疑応答などをすることはできないので、別の機会で行うことを考慮した方がよいでしょう。

### 会期中は会場3Fの和室をハッカソンなどのために開放します

日本Ruby会議2010でやった企画のように、「数人の発表者 + 質疑応答」というスタイルを実現できるかもしれません。あるいは、その場で一緒にコードを書いたりすることもできるかもしれません。

または、Lightning Talkと組み合わせて質疑応答をこちらで行うということも考えられます。

### 会期中にアンカンファンレス、!RubyKaigi(NotRubyKaigi)2011を開催しておきます

Lightning Talkより自由度が高いので、この時間だけで「発表 + 質疑応答」ができるかもしれません。

### RubyKaigi Advent Calendar 2011

日本Ruby会議 2011の会期中ではなく、それより前に場を設けるという方法です。他の方法より準備が大変そうですが、一番自由度は高くなります。いろいろやりたいことがある場合はこの方法がよいかもしれません。

### まとめ

日本Ruby会議2011でRubyとLDAPの話をする場を設ける方法案を紹介してみました。興味が出てきた方は[[activeldap:88] 今年も日本Ruby会議2011に参加しませんか？](http://rubyforge.org/pipermail/ruby-activeldap-discuss-ja/2011-June/000087.html)へどうぞ。

（どの方法案もLDAPに関係ないような気がします。）
