---
tags:
- presentation
title: 'Speee Cafe Meetup #07：OSSの開発に参加しよう！ - OSS Gate #speee_lounge #oss_gate'
---
須藤です。今月はあと2つ発表があります。（[関西Ruby会議2017](http://regional.rubykaigi.org/kansai2017/)と[Apache Arrow勉強会](https://classmethod.connpass.com/event/56478/)。）
<!--more-->


2017年5月23日に[Speee Cafe Meetup #07](https://speee.connpass.com/event/56197/)が開催されました。「OSS開発」がテーマということだったので、「OSSの開発に参加しよう！」という話をしました。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/speee-cafe-meetup-07/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/speee-cafe-meetup-07/" title="OSSの開発に参加しよう！ - OSS Gate">OSSの開発に参加しよう！ - OSS Gate</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/speee-cafe-meetup-07/)

  * [スライド（SlideShare）](https://www.slideshare.net/kou/speeecafemeetup07)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-speee-cafe-meetup-07)

### 内容

この発表では次のことを紹介しました。

  * 「OSSの開発に参加」というとガリガリ難しいコードを書くというイメージがあるかもしれないけど、こんなことやあんなことも「OSSの開発に参加」だよ。

  * 「OSSの開発に参加したい！」という気持ちはあるけどまだ行動できていない人のために、「OSSの開発に参加」をサポートする仕組みがあるよ。

こんなことやあんなことも「OSSの開発に参加」だよ、の具体例として次の例を紹介しました。

  * gemのインストールが失敗したとき：それを開発元にフィードバックする

  * ドキュメントにtypoがあった：それを開発元にフィードバックする

  * 使っているgemに機能が足りない：それを開発元にフィードバックする

関連しそうな話として「OSSのエコシステムに参加」ってどういうことだろう？という話もしました。「OSSのエコシステムに参加」の私の説明はこうです。

> 自分たちのソフトウェアもオープンソースのソフトウェアも同じように扱うこと


問題があれば直すし、気になることがあれば共有する。つまり、いつも自分たちのソフトウェアでやっているようにオープンソースのソフトウェアでのやればいいんです。

「OSSの開発に参加したい！」という気持ちはあるけどまだ行動できていない人のために、[OSS Gate](https://oss-gate.github.io/)と[OSS開発支援サービス]({% post_url 2016-06-27-index %})を紹介しました。個人的にOSSの開発に参加したいという方はOSS Gateの方が向いています。業務の中でOSSの開発に参加したいという方はOSS開発支援サービスの方が向いています。（あるいは、個人的にOSS Gateに参加して、そこで得た知見を業務に活かす、というやり方もあります。）

OSS Gateの方に興味がある人は今週の土曜日（5月27日）に[OSS Gate東京ワークショップ2017-05-27](https://oss-gate.doorkeeper.jp/events/59202)があるのでそれに参加することをオススメします。

OSS開発支援サービスの方に興味がある人は、[Speeeさんでの事例]({% post_url 2017-05-17-index %})を参考にしつつ、[お問い合わせ](/contact/?type=oss-development)ください。

### まとめ

2017年5月23日開催の[Speee Cafe Meetup #07](https://speee.connpass.com/event/56197/)でOSSの開発に参加しよう！という話をしてきました。業務でOSSの開発に参加したい、という方には、私の話以外の話も興味深いと思います。すでにいくつか資料が公開されているのでそちらも見てみてください。

  * [マイクロサービスとOSSのおいしい関係](https://www.slideshare.net/fumiyashinozuka/oss-76279514) by FiNKの[shinofumijp](http://qiita.com/shinofumijp@github)さん

  * [OSS開発が業務にもたらす恩恵](https://speakerdeck.com/pataiji/osskai-fa-gaye-wu-nimotarasuen-hui) by Speeeの[@pataiji]さん

  * [クローズドソースから始めるオープンソース](https://www.slideshare.net/takafumionaka/ss-76268823) by ドリコムの[@onk](https://twitter.com/onk)さん
