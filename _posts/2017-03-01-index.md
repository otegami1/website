---
tags: []
title: 'OSS Gate大阪ワークショップ2017-02-25を開催 #oss_gate'
---
2017年2月25日に[ファーストサーバさん](https://www.firstserver.co.jp/)で[OSS Gate大阪ワークショップ](https://oss-gate.doorkeeper.jp/events/56141)を開催しました。初の大阪開催です！
<!--more-->


![集合写真]({{ "/images/blog/20170301_0.jpg" | relative_url }} "集合写真")

参加者は26名で東京開催並の規模になりました。大阪だけでなく、神戸・京都など近隣の人たちも集まっていました。

今回は進行の様子を動画に撮ることにチャレンジしました。撮影は[@yasulab](https://twitter.com/yasulab)で、東京から日帰りで参加しました。ありがとうございます！

音声面での課題はありますが、録音機材の整備や字幕の追加などでカバーできそうです。ワークショップの最初の「OSS Gateの紹介」部分を字幕付きで公開しているので、雰囲気を知りたい方は確認してみてください！

<div class="youtube">
  <iframe width="560" height="315" src="//www.youtube.com/embed/0zeO-9M0i_w?cc_load_policy=1" frameborder="0" allowfullscreen></iframe>
</div>


今回のワークショップもビギナー・メンターともに楽しんで参加していたようです。次回も参加するという方が多かったので、次回も楽しみですね！

次回の関西でのワークショップ開催は4月になりそうです。興味のある方は[チャット](https://gitter.im/oss-gate/kansai)に参加してください。最新情報を入手できます。

現在は「OSS Gateワークショップ」は東京・札幌・大阪で開催しています。自分たちの地域でも開催したい！という方は[チャット](https://gitter.im/oss-gate/general)で相談しましょう！
