---
tags: []
title: WerckerのDockerバックエンドを使ってCentOS向けCIをするには
---
クリアコードでは[Hatohol](http://www.hatohol.org)というソフトウェアの開発に参加しています。Hatoholは複数の統合監視システムの情報を一括して表示することを可能とするソフトウェアです。現在対応している統合監視システムはZabbix、Nagios及びOpen StackのCeilometerです。他の監視システムに対応することも検討しています。
<!--more-->


HatoholはServerとDjangoのWebApp、またユーザーのブラウザで動くJavaScript部分の３つで構成されています。

今回はHatohol serverのCIについてです。

HatoholプロジェクトではTravis CIを用いてCIをしています。Travis CIではUbuntu Serverが使われています。そのため、Ubuntuではビルドが通る事は確認できていました。しかし、CentOS 6系でもビルドが出来るかどうかのCIの仕組みがHatoholプロジェクトには入っていませんでした。

### Werckerとは

[Wercker](http://wercker.com)とはTravis CIやCircle CIと並ぶCIサービスです。特に、workerが使うコンテナを自由にユーザーが用意する事ができ、自由度が高いのが特徴です。

今回Werckerが [Dockerに対応したバックエンドを公開](http://devcenter.wercker.com/articles/docker/)したので、HatoholのCentOS 6系向けCIがこのバックエンドで出来るかどうかを試してみました。

### Werkcerの設定方法

WerckerはWerckerのアカウントを作成する方法と、GitHub認証によりWerckerと連携する方法があります。どちらの方法でも使い始める事が出来ます。

Werckerを使うにはリポジトリの直下にwercker.ymlを配置する必要があります。

また、WerckerでAppと呼ばれるものを作る必要があります。
このAppを起点にしてCIが始まります。[^0]

今回はWerckerのDockerバックエンドを試すので、`wercker-labs/docker` のbox [^1] を使ったwercker.ymlを作成します。

```yaml
box: wercker-labs/docker
build:
  steps:
    - script:
        name: docker version
        code: |
        docker pull centos:6.6
```


このようにしてやる事で、WerckerのDockerバックエンドを使ったboxがWerckerの中で動き出します。

### Dockerfileを用いてDockerバックエンドworkerを走らせる

WerckerのDockerバックエンドでworkerを動かす際には、通常通りDockerfileを用いてdockerコンテナをビルドする事が可能です。

先ほどのwercker.ymlを次のように書き換えます。

```yaml
box: wercker-labs/docker
build:
  steps:
    - script:
        name: docker version
        code: |
        docker build -t centos6-wercker .
```


また、リポジトリのwercker.ymlと同じ階層に `Dockerfile` を配置します。

```dockerfile
from centos:6.6

maintainer clear-code

RUN rpm -ivh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
```


ここまでで、次のようなリポジトリ構造になっています。

```bash
    $ tree
    .
    |-- Dockerfile
    `-- wercker.yml
```


ここまでの変更をpushしてみましょう。するとWerckerのworkerが動き出します。

### HatoholプロジェクトでのWercker

HatoholプロジェクトではCentOS6.6のコンテナを用い、Hatoholのrpmビルドが通るかどうかのCIにWerckerを使い始めました。始めはビルドが通るかだけのCIだったのですが、ビルドの時間制限の25分を超えずにビルドが終了していたのでrpmのビルドも行うようにしました。
25分に制限されているというのは

[Wercker - blog](http://blog.wercker.com/2013/06/24/Opening-up-registrations.html)

> .. you’re allowed to have any number of applications on wercker with a maximum of two concurrent builds, limited at 25 minutes per build each.


によります。アプリケーション数は無制限、アプリケーションのビルドは最大2並列まで、１ビルドにつき25分の制限、という制限が伺えます。

Hatoholプロジェクトでは次のような `wercker.yml` を作成し、

```yaml
box: wercker-labs/docker
build:
  steps:
    - script:
        name: Build a CentOS 6.6 Docker Hatohol image box
        code: |
          docker -v
          docker build -t centos66/build-hatohol wercker/
```


`wercker.yml` とは同じ階層ではなくwerckerディレクトリの下にDockerfileを配置しています。

```dockerfile
from centos:6.6

maintainer hatohol project

# install libraries for Hatohol
RUN yum install -y glib2-devel libsoup-devel sqlite-devel mysql-devel mysql-server \
  libuuid-devel qpid-cpp-client-devel MySQL-python httpd mod_wsgi python-argparse
# setup mysql
RUN echo "NETWORKING=yes" > /etc/sysconfig/network
RUN rpm -ivh --force http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
RUN yum install -y librabbitmq-devel wget
RUN wget -P /etc/yum.repos.d/ http://project-hatohol.github.io/repo/hatohol.repo
RUN yum install -y json-glib-devel qpid-cpp-server-devel
# setup qpid
RUN yum groupinstall -y 'Development tools'
RUN yum install -y Django
RUN rpm -Uvh http://sourceforge.net/projects/cutter/files/centos/cutter-release-1.1.0-0.noarch.rpm
RUN yum install -y cutter
# git clone
RUN git clone https://github.com/project-hatohol/hatohol.git ~/hatohol
# build
RUN cd ~/hatohol && libtoolize && autoreconf -i
RUN cd ~/hatohol && ./configure
RUN cd ~/hatohol && make -j `cat /proc/cpuinfo | grep processor | wc -l`
RUN cd ~/hatohol && make dist -j `cat /proc/cpuinfo | grep processor | wc -l`
# rpm build
RUN yum install -y rpm-build
RUN cd ~/hatohol && MAKEFLAGS="-j `cat /proc/cpuinfo | grep processor | wc -l`" rpmbuild -tb hatohol-*.tar.bz2
```


これにより、WerckerでCentOS 6.6コンテナが動き出します。

しばらく経つと、Dockerコンテナのビルドが完了します。

### まとめ

WerckerのDockerバックエンドでCentOS 6系向けのCIの方法を解説しました。

Ubuntu Serverを用いたCIサービスは数多くありますが、CentOSの選択肢があるCIサービスはまだまだ少ないのが現状です。

Dockerコンテナ化されているLinuxディストリビューションであればDockerが動くCIサービスを用いて任意のディストリビューション向けのCIがDockerの上で出来るようになることが期待されます。

Ubuntu Server向けではなく、CentOS向けにもCIしたくなった時にWerckerのDockerバックエンドを用いてCentOSのDockerコンテナ上でCIを行う事を選択肢の一つとして検討してみるのはいかがでしょうか。

[^0]: 何故Appと言われるかというとWerckerはCIだけではなくデプロイも含めたCIサービスだからです。

[^1]: werckerではworkerのジョブを走らせるコンテナをboxと呼びます。
