---
tags:
- groonga
title: 全文検索エンジンgroongaを囲む昼下がりと夕べのお知らせ
---
今月も[全文検索エンジンgroonga](http://groonga.org/ja/)と、groongaをMySQLから使うためのモジュールである[groongaストレージエンジン](http://mroonga.github.com/)がリリースされました。
<!--more-->


  * [groongaの変更点](http://sourceforge.jp/projects/groonga/lists/archive/dev/2011-September/000565.html)
  * [groongaストレージエンジンの変更点](http://www.mysql.gr.jp/mysqlml/mysql/msg/15635)

そして、groonga勉強会の開催が決まりました。

  * [全文検索エンジンgroongaを囲む昼下がり@札幌](http://atnd.org/events/20451): 2011-10-08
  * [全文検索エンジンgroongaを囲む夕べ 2 #groonga](http://atnd.org/events/20446): 2011-11-29

昼下がりの方は来週の土曜日に札幌で開催されます。夕べの方は2ヶ月後の29日に東京で開催されます。夕べは昨年同じ日にちに開催したgroonga勉強会の第2回目という位置づけで、前回と同様に開発している側からのgroongaと関連プロダクトの説明が主になります。一方、昼下がりの方は時間的なゆとりがあることもあり、単に説明を聞くだけではなく、質疑応答にも十分な時間をとれそうです。

groongaに興味のある方はぜひご参加ください。
