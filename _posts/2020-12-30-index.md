---
title: CVEのレポートにフィードバックを送るには
author: kenhys
---


脆弱性の報告はよくCVE(Common Vulnerabilities and Exposures)として言及されます。
個別の脆弱性について https://cve.mitre.org/ へ報告したことはなくても、閲覧したことはあるのではないでしょうか。

今回は、CVEの内容が古くなっていたりしたときにフィードバックする方法を紹介します。

<!--more-->

### フィードバックの方法について

以下では、採番されたCVEのReferencesが古かった場合にフィードバックする手順を説明します。

* [Contact Us](https://cve.mitre.org/about/contactus.html)をクリックする
* フォームのSelect a request typeは「Request an update to an existing CVE Entry」を選択する
* Enter your e-mail addressに連絡先のアドレスを入力する
* Type of update requestedから「Update references」を選択する
* CVE ID to be updatedに対象のCVE-XXXX-XXXXXを入力する
* 「Describe update and rationale for requesting and update, and include new descriptions」の欄に更新内容を記述する

適切に説明を英語で記述する以外は、難しいことはありません。
送信すると、連絡先のメールアドレスへと以下のような通知が届きます。

```
Thank you for your submission. It will be reviewed by a CVE Assignment Team member.

You have requested an update to the following published CVE:  CVE-XXXX-XXXXX


Changes, additions, or updates to your request can be sent to the CVE Team by replying directly to this email.

Please do not change the subject line, which allows us to effectively track your request.

CVE Assignment Team

M/S M300, 202 Burlington Road, Bedford, MA 01730 USA

[A PGP key is available for encrypted communications at

http://cve.mitre.org/cve/request_id.html]
```


### まとめ

今回は、CVEの内容が古くなっていたりしたときにフィードバックする方法を紹介しました。
開発しているソフトウェアの脆弱性が報告されているが、その内容が古かったりして訂正する必要がある場合には参考にしてみてください。
