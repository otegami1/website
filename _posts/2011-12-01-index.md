---
tags:
- groonga
- presentation
title: '全文検索エンジンgroongaを囲む夕べ 2: 「groonga村」と「mroongaのベンチマーク」用資料'
---
今年も11月29日に「[全文検索エンジンgroongaを囲む夕べ](http://atnd.org/events/20446)」が開催されました。1年ぶりの開催です。会場は[株式会社VOYAGE GROUP](http://voyagegroup.com/)（10月に株式会社ECナビから社名変更）でした。会場提供ありがとうございます！とても助かりました。会場提供にあたり[こしばさん](http://pics.livedoor.com/u/kushii_/9066002)にとてもお世話になりました。ありがとうございます。
<!--more-->


  * [資料一覧](http://groonga.org/ja/publication/): まとめ中
  * [Ustreamの録画](http://www.ustream.tv/recorded/18817014): [グニャラくんさん](https://twitter.com/tasukuchan)がUstream係をやってくれました。ありがとうございます。
  * [Twitter上での反応](http://togetter.com/li/220833): [とみたさん](https://twitter.com/tmtms)がまとめてくれいました。ありがとうございます。

29日なので、もちろん新しいバージョンのリリースも行われています。

  * [groonga 1.2.8](http://groonga.org/ja/blog/2011/11/29/release.html)
  * [mroonga 1.10](http://mroonga.github.com/ja/blog/2011/11/29/release.html)
  * [rroonga 1.3.0](http://groonga.rubyforge.org/index.html.ja#about-rroonga)
  * [ActiveGroonga 1.0.7](http://groonga.rubyforge.org/index.html.ja#about-active-groonga)

今年は、会の内容の概要を紹介する導入用のセッション「groonga村」と、[mroonga](http://mroonga.github.com/)の性能特性がわかるベンチマーク結果を紹介するセッション「mroongaのベンチマーク」を担当しました。

[![groonga村]({{ "/images/blog/20111201_0.png" | relative_url }} "groonga村")](/archives/groonga-night-2-introduction/)

[![mroongaのベンチマーク]({{ "/images/blog/20111201_1.png" | relative_url }} "mroongaのベンチマーク")](/archives/groonga-night-2-mroonga-benchmark/)

「mroongaのベンチマーク」で紹介した結果は[OSC2011.DBで紹介した結果]({% post_url 2011-11-07-index %})とほとんど同じなので、解説はそちらを参照してください。ただし、OSC2011.DBで紹介した位置情報検索のベンチマークデータが間違っていました。

[![間違っていた位置情報検索のベンチマーク結果]({{ "/images/blog/20111201_2.png" | relative_url }} "間違っていた位置情報検索のベンチマーク結果")](/archives/groonga-night-2-mroonga-benchmark/mroonga-benchmark-06.html)

正しくは、「mroongaの方が劇的に速い」という結果ではなく、以下のように「mroongaの方が速い」という結果でした。

[![正しい位置情報検索のベンチマーク結果]({{ "/images/blog/20111201_3.png" | relative_url }} "正しい位置情報検索のベンチマーク結果")](/archives/groonga-night-2-mroonga-benchmark/mroonga-benchmark-05.html)

ここでは、groonga周辺の技術の整理にもなるので「groonga村」の内容を紹介します。

### groonga開発チームの期待

まずはじめにこの会でgroonga開発チームが参加者のみなさんに期待していることを伝えました。

[![groonga開発チームの期待]({{ "/images/blog/20111201_4.png" | relative_url }} "groonga開発チームの期待")](/archives/groonga-night-2-introduction/groonga-village-03.html)

期待していることは以下の2つです。

  * groonga開発者が増えること
  * groongaユーザーが増えること

どちらもgroongaがよくなることにつながります。

[森さんの資料](http://groonga.org/ja/publication/presentation/groonga-night-2-newyear-harvest.pdf)（PDF）や[矢田さんの資料](http://groonga.org/ja/publication/presentation/groonga-night-2-the-future.pdf)（PDF）にもあるとおり、groonga開発チームでやりたいことはいろいろあるのですが、そこまで手がまわっていません。groongaを開発してくれる人が増えると、groongaがより使いやすくより速い全文検索エンジンになります。

ユーザーが増えるといろんな環境でのテストにもなります。問題があったとフィードバックをもらって、それを修正できればgroongaの品質が向上します。また、実際に使うにはどのような機能が求められているかを聞かせてもらえれば、よりgroongaを使いやすくすることにも役立ちます。それ以外にも「このように使っています」や「groongaを使うために便利なツールを作りました」などといった情報を公開してくれるというのも、groongaユーザーにとって有益な情報になります。ぜひ、使って、フィードバックや情報公開をお願いします！

groongaの開発に参加したいという方も、[groongaの事例紹介ページ](http://groonga.org/ja/users/)に載せてもいいという方も、[groongaのメーリングリスト](http://lists.sourceforge.jp/mailman/listinfo/groonga-dev)またはgroonga at razil.jp（またはkou@clear-code.com）へご連絡ください！

### 参加者の期待

今年の会は、参加者のみなさんが以下を期待しているのではないかということで構成しました。

[![参加者のみなさんの期待]({{ "/images/blog/20111201_5.png" | relative_url }} "参加者のみなさんの期待")](/archives/groonga-night-2-introduction/groonga-village-04.html)

それぞれについて以下のセッションで扱いました。資料は公開できるように調整中です。すでに公開されているものはリンクしています。まだ資料が公開されていないものは[Ustreamの録画](http://www.ustream.tv/recorded/18817014)を観てください。

  * もう使っていいくらいgroongaは安定しているのか
    * Geographical Searching: [株式会社ぐるなび](http://www.gnavi.co.jp/)の塩畑さん
    * mroongaのベンチマーク（前述）
    * [mroongaの未サポート機能（2011/11/29時点）](http://www.slideshare.net/Kentoku/mroonga-unsupported-feature20111129): mroonga開発者の斯波さん
  * 前回の会からどのくらい進化しているのか
    * [新年と収穫の祭り](http://groonga.org/ja/publication/presentation/groonga-night-2-newyear-harvest.pdf)（PDF）: groonga創始者の[森さん](https://twitter.com/daijiro)
    * [mroonga](http://www.slideshare.net/Kentoku/introducing-mroonga-20111129): mroonga開発者の[斯波さん](http://wild-growth-ja.blogspot.com/)
    * groonga with PostgreSQL: [フォルシア株式会社](http://www.forcia.com/)の[奥野さん](https://twitter.com/choplin)
  * groongaは高速化のために内部でどのように頑張っているのか
    * [groonga開発予報](http://groonga.org/ja/publication/presentation/groonga-night-2-the-future.pdf)（PDF）: [有限会社未来検索ブラジル](http://razil.jp/)の[矢田さん](https://twitter.com/s5yata)
  * groongaを使っていて困っていることがあるから聞きたい
    * [株式会社VOYAGE GROUPの社内ステキバーのAJITO](http://blog.kushii.net/archives/1675844.html)で行う懇親会

### groonga関連ソフトウェアの位置づけ

groongaはライブラリとして利用できるため、MySQLやPostgreSQL、Rubyなど様々なソフトウェアと連携できます。そのため、今回の会でもたくさんの分野の話がでてきます。話をきいているうちにどこの話をしているのかわからなくなってしまうと、話についていけなくなってしまうかもしれません。それを防ぐためにgroongaとその関連ソフトウェアの位置づけを説明しました。

[![groonga村]({{ "/images/blog/20111201_6.png" | relative_url }} "groonga村")](/archives/groonga-night-2-introduction/groonga-village-18.html)

これが全体像です。それでは、それぞれの部分を説明します。

### コア機能

[![コア機能]({{ "/images/blog/20111201_7.png" | relative_url }} "コア機能")](/archives/groonga-night-2-introduction/groonga-village-20.html)

キー管理機能（レコードを参照するときに利用）や転置索引、キーストアなどgroongaが提供する機能のベースになる機能を提供するレイヤーがあります。

### DB API

[![DB API]({{ "/images/blog/20111201_8.png" | relative_url }} "DB API")](/archives/groonga-night-2-introduction/groonga-village-22.html)

コア機能の上にデータベース機能を提供するDB APIがあります。このAPIを使うとSQLiteのようにプロセス内にデータベースを持ったアプリケーションを開発することができます。

### rroonga

[![rroonga]({{ "/images/blog/20111201_9.png" | relative_url }} "rroonga")](/archives/groonga-night-2-introduction/groonga-village-23.html)

DB APIを使ったソフトウェアが[rroonga](http://groonga.rubyforge.org/index.html.ja#about-rroonga)です。RubyからDB APIを使えるようになります。[buzztter](http://buzztter.com/)や[るりまサーチ](http://rurema.clear-code.com/)、[Milkode](http://milkode.ongaeshi.me/wiki/)などがrroongaを使っています。

### mroonga

[![mroonga]({{ "/images/blog/20111201_10.png" | relative_url }} "mroonga")](/archives/groonga-night-2-introduction/groonga-village-25.html)

MySQLからDB APIを使うためのソフトウェアが[mroonga](http://mroonga.github.com/)です。SQLを使ってgroongaを使えるようになるため、既存のSQL関連の技術をそのまま使えることが魅力です。例えば、Ruby on RailsでWebアプリケーションを使っている場合はActiveRecord経由でgroongaの高速な全文検索機能を使えることになります。

### クエリAPI

[![クエリAPI]({{ "/images/blog/20111201_11.png" | relative_url }} "クエリAPI")](/archives/groonga-night-2-introduction/groonga-village-29.html)

groongaは独自のクエリ言語を持っています。RDBMSでいうSQLのようなものです。このAPIを使うと文字列をやり取りすることでgroongaの機能を使うことができます。

### groongaコマンド

[![groongaコマンド]({{ "/images/blog/20111201_12.png" | relative_url }} "groongaコマンド")](/archives/groonga-night-2-introduction/groonga-village-31.html)

クエリAPIをネットワーク越しやコマンドライン上で使うためにgroongaコマンドを提供しています。groongaコマンドはHTTPサーバーにもなるため、特別なライブラリではなく通常のHTTPライブラリを使うだけで任意のプログラミング言語からgroongaの機能を使うことができます。塩畑さんの発表にもある通り、ぐるなびさんではHTTPでgroongaの機能を利用しています。

### nroonga

[![nroonga]({{ "/images/blog/20111201_13.png" | relative_url }} "nroonga")](/archives/groonga-night-2-introduction/groonga-village-33.html)

クエリAPIを[Node.js](http://nodejs.org/)から使えるようにするライブラリが[nroonga](https://github.com/nroonga/nroonga)です。Node.jsからgroongaの機能を利用するための[node-groonga](https://github.com/hideo55/node-groonga)というライブラリもありますが、こちらはgroongaコマンドのHTTPサーバーと通信するもので、直接クエリAPIを叩くnroongaとはレイヤーが違います。node-groongaはgroongaコマンドをより便利に使うためのもので、nroongaはgroongaコマンドのHTTPサーバー機能の代替になりうるものです。

groongaコマンドでもHTTPサーバー機能を実現できますが、実は単純なHTTPサーバー機能しかありません。例えば、認証機能もありませんし、WebSocketを実現することもできません。それらの機能も実現できるリッチなHTTPサーバーはNode.jsが提供する機能を利用するのがよいのではないか、というアイディアから生まれたものです。まだ開発が始まったばかりのプロジェクトですが、興味のある方はぜひ開発に参加してください！

### textsearch_groonga

[![groonga with PostgreSQL]({{ "/images/blog/20111201_14.png" | relative_url }} "groonga with PostgreSQL")](/archives/groonga-night-2-introduction/groonga-village-34.html)

PostgreSQLからgroongaの機能を使うためのソフトウェアが[textsearch_groonga](http://textsearch-ja.projects.postgresql.org/textsearch_groonga.html)です。mroongaと違いDB APIとクエリAPIをうまく組み合わせてgroongaの機能を使っています。

### まとめ

[![groonga村]({{ "/images/blog/20111201_6.png" | relative_url }} "groonga村")](/archives/groonga-night-2-introduction/groonga-village-35.html)

groongaはライブラリとして利用できるため、このように多くのソフトウェアと連携し、ニーズにあわせた使い方ができるようになっています。ぜひgroongaを使ってみてフィードバックをお願いします！

それでは、また、来年のいい肉の日に会いましょう[^0]。会場提供をしてくれたVOYAGE GROUPのみなさん、Ustreamで放送してくれたグニャラくんさん、司会をしてくれた[坂井さん](http://d.hatena.ne.jp/sakaik/)、受付をしてくれた[Ruby会議実行委員会](http://rubykaigi.org/2011/ja/team)のしまださん、すずきさん、発表者のみなさん、会場に来てくれたみなさん、Ustreamで参加してくれたみなさん、ありがとうございました！

### おまけ: 参加状況について

今回も前回同様にATNDを使ってイベント告知・参加者登録を行いました。定員100名で告知をし、前日までに160名くらいの登録でキャンセル数は1桁でした。ですが、前回の当日キャンセル数とキャンセルなしで不参加の傾向を考えると、事前に160名の参加登録があったとしても当日はちょうど定員である100名くらいになるのではないかと予想し、「補欠の人も会場にきても大丈夫です」ということにしました。実際、当日にキャンセル数が30くらいになり、キャンセルなしで不参加の人も50人くらいいたため、最終的には80-90名くらいの参加になりました。（当日に参加登録した方もいたようです。）

「補欠の人も会場にきても大丈夫です」というスタイルは会場提供のVOYAGE GROUPさんがOKを出してくれたので実現できたのですが、このくらいのゆるさでやったほうがちょうどよくなるのかもしれませんね。柔軟な対応、ありがとうございました！

[^0]: groonga開発チーム主催のgroonga勉強会は年に一度ですが、ユーザーのみなさんはいい肉の日以外にもgroonga勉強会を開催して大丈夫です！
