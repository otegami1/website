---
tags:
- presentation
title: 'OSS Gate東京ワークショップ2017-07-29を開催 #oss_gate'
---
東京のワークショップでは久しぶりに進行役をした須藤です。
<!--more-->


2017年07月29日に[クラウドワークスさん](https://crowdworks.jp/)で[OSS Gate東京ワークショップ](https://oss-gate.doorkeeper.jp/events/61378)を開催しました。そういえば、今回は集合写真を撮り忘れましたね。。。

数回前から[YassLab](https://yasslab.jp/ja)の[安川さん](https://twitter.com/yasulab)さんが進行の録画にチャレンジしていましたが、今回はかなり完成度の高いものができました！[YouTubeでOSS Gate ワークショップ (完全収録版)というプレイリスト](https://www.youtube.com/playlist?list=PLg41HKRyxE6gIvt3sM09REYvLYkRRkuaS)を公開しているので、どんな様子か知りたい方は見てみてください。

また、今回から[説明用の資料を1つのスライドにまとめ](https://slide.rabbit-shocker.org/authors/oss-gate/workshop-tutorial/)ました。サポーター向けの説明が足りなそうという課題はありますが、これまでより進行がしやすくなりました。

あと、今回は作業時間を1時間+1時間に短くしてみました。（これまでは1時間+2時間30分とか。）同じ日に開催していた[OSS Gate京都ワークショップ](https://oss-gate.doorkeeper.jp/events/62572)でも同じような時間配分だったりします。やってみて、アンケートで参加者に感触を聞いた感じでは、進め方次第では1時間でもいけそうな感触を得ました。

今月は[大阪でワークショップ](https://oss-gate.doorkeeper.jp/events/63163)、[東京でミートアップ](https://oss-gate.doorkeeper.jp/events/63053)が開催されます。

来月は[高専生向けのワークショップが東京で開催](https://oss-gate.doorkeeper.jp/events/63111)されます。（メインターゲットが高専生なだけで高専生以外も参加できます。）

OSS Gateワークショップに興味がある方は一度どこかのイベントに参加して、参加者の人と話をしてみるとよいと思います。実際に自分で体験することで理解が深まりますし、まわりの人と話すことで協力しやすくなります。

企業で開催したい（社員がOSSの開発に参加することを支援したい）場合はクリアコードに[お問い合わせ](/contact/?type=oss-development)ください。有償で平日日中での開催を支援しています。
