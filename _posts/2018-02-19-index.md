---
tags:
- mozilla
title: Firefox ESR60以降でのPolicy Engineによるポリシー定義の手順
---
Firefox ESR60以降のバージョンでは、[Policy Engineと呼ばれる新しいポリシー設定の仕組み](https://blog.mozilla.org/futurereleases/2018/01/11/announcing-esr60-policy-engine/)が導入されます。近いうちに公式なドキュメントが用意されるものと予想されますが、Firefoxを法人で利用中の場合、一日も早く実際の動作を検証したいというニーズもある事でしょう。この記事では、現時点で実装されているPolicy Engineの具体的な使用手順を解説します。
<!--more-->


### 準備

Policy EngineはNightly 60で既に使用可能な状態になっています。[Firefox開発版のダウンロードページ](https://www.mozilla.org/ja/firefox/channel/desktop/)から最新の開発版であるNightlyをダウンロードし、インストールしておきます。

次に、Nightlyの起動用ショートカットを編集します。Firefoxは通常版と開発版（Nightly）でプロファイルの内容に互換性が無い場合があり、普段使いのFirefoxのプロファイルでNightlyを起動してしまうと、不可逆的な移行処理が行われてしまって、以後通常版のFirefoxでそのプロファイルを使用できなくなる可能性があります。Firefoxのショートカットのプロパティを開き、「リンク先」欄の末尾に`C:\Program Files\Nightly\firefox.exe -profile %temp%\NightlyProfile -no-remote`のように起動プロファイルを明示しておく事で、普段使いのプロファイルとは別の専用プロファイルでNightlyを起動できるようになります。

Nightlyの準備が終わったら、Policy Engineの設定の準備です。

<!--
Policy Engineを有効化するためには、[MCD（AutoConfig）](https://developer.mozilla.org/ja/docs/MCD,_Mission_Control_Desktop_AKA_AutoConfig)で以下の設定を反映します。

```javascript
lockPref("browser.policies.enabled", true);
// 詳細なログを出力する場合は以下も設定する
// lockPref("browser.policies.loglevel", "debug");
```
-->


Policy Engine用のポリシー設定は、Windowsの場合はレジストリ経由（グループポリシーオブジェクトでの設定）とJSONファイル経由での設定の2通りの方法があります。
簡単のため、ここではJSONファイルを使う方法のみ解説します。

ポリシー設定のためのJSONファイルは、Firefoxのインストール先フォルダ配下に`distribution`という名前でフォルダを作成し、さらにその中に`policies.json`という名前で以下の内容のテキストファイル（JSON形式）を設置します。

```javascript
{
  "policies": {
  }
}
```


以上で準備は完了です。

### ポリシー設定の記述の仕方

JSONファイルでのポリシー設定は`policies.json`の`policies`のプロパティとして記述します。現時点で使用できるポリシー設定にどのような物かがあるかは[`policies.json`のスキーマ定義](https://dxr.mozilla.org/mozilla-central/source/browser/components/enterprisepolicies/schemas/policies-schema.json)に列挙されています。

例えば、`about:config`で設定を変更される事とFirefoxの自動更新のそれぞれを禁止したい場合、`policies.json`は以下のように記述します。

```javascript
{
  "policies": {
    "BlockAboutConfig": true,
    "DisableAppUpdate": true
  }
}
```


2018年3月9日現在、以下のポリシー設定が存在します。これらは仕様が変更または削除される可能性がある事、あるいは新しいポリシー設定が今後追加される可能性がある事に注意して下さい。

  * `"BlockAboutAddons": true` ：`about:addons`（アドオンマネージャ）の使用を禁止する。間接的に、アドオンのインストールを禁止する効果がある。

  * `"BlockAboutConfig": true` ：`about:config`の使用を禁止する。同時に、副作用として`"DisableDeveloperTools": true`の効果も反映される。

  * `"BlockAboutProfiles": true` ：`about:profiles`の使用を禁止する。

  * `"BlockAboutSupport": true` ：`about:support`の使用を禁止する。

  * `"BlockSetDesktopBackground": true` ：画像をコンテキストメニューからデスクトップの壁紙に設定する機能の使用を禁止する。

  * `"Bookmarks": [{"Title": "...", "URL": "...", "Favicon": "...", "Placement": "toolbar", "Folder": true/false }, ...]` ：ブックマークツールバーに既定のブックマーク項目を追加する。

  * `"Bookmarks": [{"Title": "...", "URL": "...", "Favicon": "...", "Placement": "menu", "Folder": true/false }, ...]` ：ブックマークメニューに既定のブックマーク項目を追加する。

  * `"Cookies": { "Allow": ["http://example.com", "https://example.org:8080"] }` :指定のWebサイト（オリジンで指定）でCookie、IndexedDB、Web Storage、およびService Worker用Cacheを保存する（任意に無効化はできない）。

  * `"Cookies": { "Block": ["http://example.com", "https://example.org:8080"] }` :指定のWebサイト（オリジンで指定）でCookie、IndexedDB、Web Storage、およびService Worker用Cacheを保存しない（任意に有効化はできない）。また、これらのホストに保存済みのCookieがあった場合、それらは削除される。

  * `"CreateMasterPassword": false` : マスターパスワードを設定する事を禁止する。

  * `"DisableAppUpdate": true` ：Firefoxの自動更新を停止する。

  * `"DisableDeveloperTools": true` ：開発ツールの使用を禁止する。

  * `"DisableFirefoxAccounts": true` ：Firefoxアカウントの使用を禁止する（ひいては、Firefox Syncの使用も禁止される）。

  * `"DisableFirefoxScreenshots": true` ：Firefox Screenshotsの使用を禁止する。

  * `"DisableFirefoxStudies": true` ：[Firefoxの新機能のテストへの参加](https://support.mozilla.org/ja/kb/shield)を禁止する。

  * `"DisableFormHistory": true` :フォームの入力履歴の保存とオートコンプリートを禁止する。

  * `"DisablePocket": true` :Pocketの使用を禁止する。

  * `"DisablePrivateBrowsing": true` :プライベートブラウジング機能の使用を禁止する。

  * `"DisableSysAddonUpdate": true` :システムアドオンの更新を禁止する。

  * `"DisplayBookmarksToolbar": true` ：初期状態でブックマークツールバーを表示する。（ただしこの設定は強制でなく、ユーザーが任意に非表示にする事もでき、非表示にした場合は次回以降の起動時も非表示のままとなる。）

  * `"DisplayMenuBar": true` ：初期状態でメニューバーを表示する。（ただしこの設定は強制でなく、ユーザーが任意に非表示にする事もでき、非表示にした場合は次回以降の起動時も非表示のままとなる。）

  * `"DontCheckDefaultBrowser": true` :起動時に既定のブラウザにするかどうかを確認しない。

  * `"FlashPlugin": { "Allow": ["http://example.com", "https://example.org:8080"] }` :指定のWebサイト（オリジンで指定）でAdobe FlashをClick to Play無しで自動実行する（任意に無効化はできない）。

  * `"FlashPlugin": { "Block": ["http://example.com", "https://example.org:8080"] }` :指定のWebサイト（オリジンで指定）でAdobe Flashの実行を禁止する（任意に有効化はできない）。

  * `"Homepage": { "URL": "http://example.com", "Locked": true/false, "Additional": ["https://example.org:8080", ...] }` :既定のホームページを設定する。`"Locked"`が`true`の場合はホームページを固定する。また、`"Additional"`を指定した場合は2番目以降のホームページとして設定する。

  * `"InstallAddons": { "Allow": ["https://example.com", "https://example.org:8080"] }` :指定のWebサイト（オリジンで指定）でアドオンのインストール時に警告しない（`https`のみ指定可能）。

  * `"Popups": { "Allow": ["http://example.com", "https://example.org:8080"] }` :指定のWebサイト（オリジンで指定）で`window.open()`によるポップアップを常に自動的に開く（任意に無効化はできない）。

  * `"RememberPasswords": true/false` :パスワードマネージャの使用または使用禁止を強制する。

ポリシー設定はFirefoxの起動時に読み込まれます。Firefoxの動作中に変更したポリシー設定は、次回のFirefox起動時から反映されます。

### まとめ

以上、Firefox ESR60から使用可能になるPolicy Engineによるポリシー設定の手順を解説しました。

従来Firefoxでは、AutoConfigの一般的な設定でできないカスタマイズが必要な場合、アドオンやAutoConfig内に埋め込まれたスクリプトによってそれらを強引に実施するという方法を取る事ができました。ESR60以降のバージョンではXULアドオンが廃止され、またAutoConfigのスクリプト内での特権が必要なコードの実行が禁止される見込みであることから、それらの方法は取れなくなります。Policy Engineで実現可能な部分はPolicy Engineで設定するように、今のうちに備えておくようにしましょう。
