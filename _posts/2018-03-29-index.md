---
tags:
- groonga
- presentation
title: "【徳丸浩と学ぶビジネスセミナー】WordPressのセキュリティと全文検索について学ぶ！【Mroonga対応】- Mroongaの高速全文検索機能でWordPress内のコンテンツを有効活用！"
---
1ヶ月半前の話をようやくまとめている須藤です。
<!--more-->


2月9日（年に一度の肉の日！）に[【徳丸浩と学ぶビジネスセミナー】WordPressのセキュリティと全文検索について学ぶ！【Mroonga対応】](https://enterprise-wordpress.doorkeeper.jp/events/69964)でWordPressでMroongaをどう活用できるかを紹介してきました。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/enterprise-wordpress-2018-02/viewer.html"
          width="640" height="404"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/enterprise-wordpress-2018-02/" title="Mroongaの高速全文検索機能でWordPress内のコンテンツを有効活用！">Mroongaの高速全文検索機能でWordPress内のコンテンツを有効活用！</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/enterprise-wordpress-2018-02/)

  * [スライド（SlideShare）](https://slideshare.net/kou/enterprisewordpress201802)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-enterprise-wordpress-2018-02)

### 内容

対象は全文検索をあまり知らないWordPressユーザーです。そのため、全文検索とは？からまとめています。

単に全文検索とは？だけでなく、全文検索をどうやってWordPressで活用するかについてもまとめています。ザックリ言うとサイト内回遊率向上ですが、どう活用すれば向上につながるかをいろいろまとめています。

### まとめ

WordPressで全文検索を活用することについて紹介しました。ぜひ、この資料を参考にWordPressで全文検索を活用してみてください。

なんと、この続編として、2018年3月29日にWordPressのMroongaプラグインを改良しよう！というイベントがあります。

  * [【KUSANAGI × Mroonga ハンズオン】KUSANAGIでWordPressのインストールからMroongaプラグインの実装まで学べる！【中級者向け】](https://enterprise-wordpress.doorkeeper.jp/events/71673)

「WordPressのMroongaプラグイン」というのはWordPressからMroongaを使うための「WordPressのプラグイン」です。WordPressのプラグインページにも[Mroongaとして登録](https://wordpress.org/plugins/mroonga/)してあります。

「[Mroonga](http://mroonga.org/)」というのは（WordPressではなく）MySQL・MariaDBのプラグインで、MySQL・MariaDBに高速な全文検索機能を追加します。

つまり、WordPressのMroongaプラグインとはWordPressに高速な全文検索機能を追加するもの、ということです。

WordPressの全文検索で困ったことがあり、有償サポートが必要な場合は、[お問い合わせ](/contact?type=groonga)ください。
