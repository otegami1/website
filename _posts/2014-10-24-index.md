---
tags:
- presentation
title: 'いろいろ考えると日本語の全文検索もMySQLがいいね！ #osc2014tk'
---
[先日お伝えした通り]({% post_url 2014-10-15-index %})、10月18日（土）に[オープンソースカンファレンス2014 Tokyo/Fall](https://www.ospn.jp/osc2014-fall/)で[いろいろ考えると日本語の全文検索もMySQLがいいね！](https://www.ospn.jp/osc2014-fall/modules/eguide/event.php?eid=6)という発表をしてきました。
<!--more-->


<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/osc-2014-tokyo-fall/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/osc-2014-tokyo-fall/" title="いろいろ考えると日本語の全文検索もMySQLがいいね！">いろいろ考えると日本語の全文検索もMySQLがいいね！</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/osc-2014-tokyo-fall/)
  * [スライド（SlideShare）](http://slideshare.net/kou/osc-2014-tokyo-fall)
  * [スライド（Speaker Deck）](http://speakerdeck.com/u/kou/p/iroirokao-erutori-ben-yu-falsequan-wen-jian-suo-momysqlgaiine)
  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-osc-2014-tokyo-fall)（スライドのソースだけでなく、スライドの中で示しているベンチマークを実行するスクリプトなども置いてあります。）

### 内容

今回の発表は「すでにMySQLを使っていて」、「全文検索についてよく知らない」けど、「日本語の全文検索を実現したい」という人向けの内容になっています。そのため、トークナイザーや転置索引といった話はあえて外しました。

MySQLを使うと、そのような詳細を知らなくてもそこそこの機能の日本語の全文検索を実現できる、ということを伝えました。

データ量が少ない場合は適切なcollationを設定すれば`LIKE`でも十分な機能と性能が得られて、運用の手間も増えません。`LIKE`で十分なら無理して専用の全文検索サーバーは必要ありません。

データ量が増えてきたら[Mroonga](http://mroonga.org/ja/)を導入することにより高速な日本語全文検索機能を手に入れられます。しかも、トークナイザーや転置索引といったことを知らなくても使えます。運用面を考えてみても、専用の全文検索サーバーを別途導入するよりかなり楽に運用できます。なぜならMySQLの運用ノウハウを使えるからです。

すでにMySQLを使っているなら、まずは`LIKE`かMroongaを使ってみて、機能面で不足を感じ始めたら検索のチューニングを始めるのがよいでしょう。

実は、Mroongaにも検索のチューニングを実施するオプションがあるので、専用の全文検索サーバーを導入する前にMroongaで経験して全文検索に関するノウハウを貯めるという方法を使えます。

スライド中でも軽く触れましたが、知らないことに手をだすときは、いきなりカンペキを求めないやり方をオススメしています。まずは実際に動かして、気になったところ（最初はいろいろあるはず）を改良しながら知識や技術を身に着けていく方法です。コツコツと改良を積み重ねていく方法です。遠回りに感じるかもしれませんが、後から振り返ってみると結構割に合っています。

別の方法として、近くに詳しい人がいたらその人に相談するという方法もオススメです。例えば、OSCに参加してブースに行って相談する、ということですね。

### ブース番

今回は、発表だけでなく、日本MySQLユーザ会のブース番もしました。普段あまり意識していないことについて質問されたときにうまく答えられなくて、自分は何を知らなかったかに気付けました[^0]。誰かに説明をする、という機会はいいものですね。みなさんもブース番をしてみてはいかがでしょうか。日本MySQLユーザ会はブース番をしてくれる人を募集しています。[日本MySQLユーザ会のメーリングリスト](http://www.mysql.gr.jp/ml.html)に参加しているとたまに募集のメールが流れるるので、そこで手をあげるとよいです。

### まとめ

OSC2014 Tokyo/Fallでの日本MySQLユーザ会の発表内容を紹介しました。MySQLで日本語を全文検索したかった人は参考にしてください。

Mroongaや全文検索についてもっと知りたくなった人は、10/27（来週の月曜日）に開催される[Groongaドキュメント読書会5](http://groonga.doorkeeper.jp/events/16395)や、11/29（いい肉の日！土曜日）に開催される[全文検索エンジンGroongaを囲む夕べ5](http://groonga.doorkeeper.jp/events/15816)へも参加してみてください。理解が深まること間違いありません！

なお、「全文検索エンジンGroongaを囲む夕べ5」の方は発表者も募集しています。

[^0]: 今回は、特にMySQL ClusterやGalera ClusterなどMySQLの分散システムのことが勉強になりました。
