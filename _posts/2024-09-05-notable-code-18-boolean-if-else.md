---
tags:
- notable-code
title: ノータブルコード18 - 真偽値を返すif/else
author: kou
---
最近はレビューしていることが多いなぁと思っている須藤です。レビューをしていると「真偽値を返す`if`/`else`」をちょいちょい見かけるなぁと思い出したので18回目の[ノータブルコード]({% post_url 2020-01-14-index %})として紹介します。[17回目のノータブルコード]({% post_url 2021-06-28-embed-lua-script-in-batch %})が2021年6月28日なので、3年強ぶりのノータブルコードです。久しぶり！

<!--more-->

「真偽値を返す`if`/`else`」というのはこういう`if`/`else`のことです。私は知らないのですが、なにか名前がついていたりします？

```c
static bool
is_odd(int x)
{
  if (x % 2 == 1) {
    return true;
  } else {
    return false;
  }
}
```

こういう`if`/`else`を見たときは`if`/`else`なしでこれでいいんじゃない？というコメントをしています。

```c
static bool
is_odd(int x)
{
  return x % 2 == 1;
}
```

それではこんな`if`/`else`について考えてみましょう。

## `if`/`else`なしのほうがよいのか

私はない方が読みやすいなぁと思います。

```c
static bool
is_odd(int x)
{
  if (x % 2 == 1) {
    return true;
  } else {
    return false;
  }
}
```

というように`if`/`else`があると、`x % 2 == 1`が`true`なら`true`を返して、そうじゃないなら`false`を返すのね、と読んでいる気がします。

一方、`if`/`else`がないとどうでしょう。

```c
static bool
is_odd(int x)
{
  return x % 2 == 1;
}
```

この場合は、`x % 2 == 1`かどうかを返すのね、と読んでいる気がします。

考えることが1つ減っているので読みやすく感じている気がします。

このケースでは`true`のときに`true`を返していますが、次のように`true`のときに`false`を返している場合はどうでしょうか。

```c
static bool
is_odd(int x)
{
  if (x % 2 == 0) {
    return false;
  } else {
    return true;
  }
}
```

読むときにもうひと手間かかっていそうな感じがしますね。

ということで、私は`if`/`else`がない方が読みやすいと感じます。

## いつも`if`/`else`がない方がよいのか

それでは、いつも`if`/`else`を使わない方が読みやすいのでしょうか。私の経験ではそういうわけでもなさそうです。

たとえば次のケースはどうでしょうか。`if`/`else`ではないですが`if`は使っています。

```c
bool
grn_obj_is_bulk(grn_ctx *ctx, grn_obj *obj)
{
  if (!obj) {
    return false;
  }

  return obj->header.type == GRN_BULK;
}
```

これは次のようにも書けます。

```c
bool
grn_obj_is_bulk(grn_ctx *ctx, grn_obj *obj)
{
  return obj && obj->header.type == GRN_BULK;
}
```

このくらいのコードならどちらでもいい気はしますが、私がコードを読む人なら、コードを書いた人の意図を違うように読みそうです。

もとのコードでは`if`を使って`NULL`のケースを除外しています。（early returnで`false`を返しています。）そのため、`NULL`のケースはこの関数では本質ではないのだろうと読みます。`return`のところにある`obj->header.type == GRN_BULK`の部分が本質なんだろうと読みます。

`return`だけにしたコードではそのまま「`obj`が`NULL`じゃなくて`obj->header.type == GRN_BULK`なときだけ`true`なんだな」と読みます。それぞれの条件の重要度の違いはあまり感じません。

ちょっとすぐに具体例を見つけられなかったのですが、`&&`と`||`が入り乱れるような複雑な条件の場合やは`if`を使ってearly returnした方が読みやすいことが多いです。

## まとめ

「真偽値を返す`if`/`else`」をちょいちょい見るなぁと思ったのでまとめてみました。
