---
tags:
- groonga
- presentation
title: 'PostgreSQL Conference Japan 2017 - PGroonga 2 - PostgreSQLでの全文検索の決定版 #pgcon17j'
---
[PostgreSQL Conference Japan 2017](https://www.postgresql.jp/events/jpug-pgcon2017)の前に[PGroonga 2のリリースアナウンス](http://groonga.org/ja/blog/2017/10/10/pgroonga-2.0.2.html)を出せた須藤です。間に合ってよかった。
<!--more-->


PostgreSQL Conference Japan 2017でPGroonga 2を紹介しました。PGroongaを使ったことがない人向けの内容です。実際、聞いてくれた人たちはほとんど使ったことがない人ばかりでした。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/postgresql-conference-2017/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/postgresql-conference-2017/" title="PGroonga 2 - PostgreSQLでの全文検索の決定版">PGroonga 2 - PostgreSQLでの全文検索の決定版</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/postgresql-conference-2017/)

  * [スライド（SlideShare）](https://slideshare.net/kou/postgresql-conference-japan-2017)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-postgresql-conference-2017)

### 内容

PostgreSQLをバックエンドに「今っぽい」全文検索システムを作るための[PGroonga](https://pgroonga.github.io/ja/)の使い方を紹介するという内容になっています。具体的には次の機能を実現する方法を紹介しています。

  * 高速全文検索

  * それっぽい順でのソート

  * 検索結果表示画面で検索キーワードをハイライト

  * 検索結果表示画面で検索キーワード周辺テキストだけを表示

  * オートコンプリート（検索キーワードを少し入力したら補完する機能）

  * 類似文書検索（ブログの検索システムなら関連エントリーの表示に使える機能）

  * 同義語展開（表記揺れの吸収とかに使える機能）

また、次のステップとして次の機能の実現方法にも触れています。

  * PDF・オフィス文書を検索対象にする方法

    * ファイルサーバーの全文検索システムで使える

  * ロジカルレプリケーション

    * 参照のスケールアウトで使える

PGroongaを使うとPostgreSQLをもっと活用できます。ぜひ活用してください！

### おまけ

今回のスライド公開のタイミングでスライド公開サイト[Rabbit Slide Show](https://slide.rabbit-shocker.org/)にスライド中のリンクをクリックできる機能を追加しました。

たとえば、[今回のスライドの最後のページ](https://slide.rabbit-shocker.org/authors/kou/postgresql-conference-2017/?page=99)には[お問い合わせ先のリンク](/contact/?type=groonga)があるのですが、このリンクをクリックするとリンクを辿れるようになっています。

これは[Poppler](http://poppler.freedesktop.org/)というPDFレンダリングライブラリー（フリーソフトウェア）とHTMLの`<map>`要素を組み合わせて実現しています。

### まとめ

PostgreSQL Conference Japan 2017で、先日リリースしたPGroonga 2を紹介しました。PGroongaも使ってPostgreSQLをどんどん活用してください！もし、PGroonga関連でなにか相談したいことがある場合は[お問い合わせ](/contact/?type=groonga)ください。
