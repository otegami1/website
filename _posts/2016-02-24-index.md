---
tags: []
title: Debianの移植作業用のインフラを借りるには
---
### はじめに

Debianプロジェクトでは、移植作業用のインフラが各アーキテクチャごとに用意されています。
それらのインフラはDebian開発者向けに提供されているものですが、開発者だけに限定されているわけではありません。
今回は、移植作業用のインフラをDebian開発者でない人が借りる方法について紹介します。
<!--more-->


### porterboxとは

移植作業用のインフラのことをporterboxと呼んでいます。それらはdebian.org配下のドメインで提供されています。
どんなものが提供されているかは[debian.org Developer Machines](https://db.debian.org/machines.cgi)から一覧できます。
移植作業用に使われるのは、この一覧のうちpurpose欄がporterboxとなっているものです。
しかし、多種多様な目的のマシンが混在しているのでややわかりづらいです。

そこで便利なのが[porterbox](https://github.com/jbernard/porterbox)コマンドです。
これはPythonで実装されていて、実行すると対応アーキテクチャやドメイン、公開範囲について知ることができます。

```
% ./porterbox
Architecture       Hostname                 Access
--------------------------------------------------------------------
armel              abel.debian.org          public
arm64              asachi.debian.org        public
kfreebsd-amd64     asdfasdf.debian.net      public (non-DSA-machine)
amd64              barriere.debian.org      public
mipsel             etler.debian.org         public
hurd-i386          exodar.debian.net        public (non-DSA-machine)
kfreebsd-amd64     falla.debian.org         public
kfreebsd-i386      fischer.debian.org       public
armhf              harris.debian.org        [unknown]
kfreebsd-i386      io.debian.net            public (non-DSA-machine)
ia64               merulo.debian.org        public
mips               minkus.debian.org        public
powerpc            partch.debian.org        public
ppc64el            pastel.debian.net        public (non-DSA-machine)
powerpc            pizzetti.debian.org      [unknown]
ppc64el            plummer.debian.org       [unknown]
sh4                sh4.g15.jp               public (non-DSA-machine)
sparc              smetana.debian.org       public
hurd-i386          strauss.debian.net       public (non-DSA-machine)
sh4                sumotsu.debian.net       public (non-DSA-machine)
armhf              turfan.debian.net        public (non-DSA-machine)
avr32              xoothais.err.no          public (non-DSA-machine)
s390x              zelenka.debian.org       public

Found 23 machines
```


### 借りるための手続きについて

実際にporterboxを借りるときの手続きは次の通りです。

  * スポンサーしてくれるDebian開発者(DD)を探す

  * porterboxのアカウント申請メールをDDに送付する

  * DDにrt.debian.orgにチケットを起票してもらう

  * adminチームにアカウントを用意してもらう

  * ゲートウェイを経由してssh鍵を登録する

  * アカウント情報が伝搬するまでしばらく待つ

#### スポンサーしてくれるDebian開発者を探す

移植作業向けにDebian開発者でなくても借りられるとはいっても、誰にでもほいほいと貸してくれるわけではありません。
というわけで、スポンサーになってくれるDebian開発者が必要です。
まわりにDebian開発者が生息していない場合は、[Debian JP Developers メーリングリスト](http://www.debian.or.jp/community/ml/openml.html)で探すか、お近くで開催されているDebian勉強会などに参加してつかまえるとよいでしょう。

#### porterboxのアカウント申請メールをDebian開発者に送付する

幸いにしてDebian開発者をつかまえることができたら、次はporterboxのアカウントの申請メールをスポンサーしてくれるDebian開発者に送ります。

[Guest Access to porter machines](https://dsa.debian.org/doc/guest-account/)に記載されているように、以下の項目を埋める必要があります。

  * 氏名

  * アカウント名

  * PGPキーのフィンガープリント。キーサーバーに登録されているものが必要です。

  * メールアドレス

  * DMUPに同意するコメント

  * どのマシンを借りたいか

  * アクセスが必要な理由

DMUPというのは、[Debian マシン使用ポリシー](https://www.debian.org/devel/dmup.ja.htm)のことです。要は目的外に使ったりしませんよというのに同意する必要があります。

#### Debian開発者にrt.debian.orgにチケットを起票してもらう

アカウント申請メールを送ったら、次はDebian開発者にチケットを起票してもらいましょう。

アカウントの申請は[rt.debian.org](https://wiki.debian.org/rt.debian.org)で管理されています。
これにアクセスできるのはDebian開発者だけなので、スポンサーをお願いしたDebian開発者にチケットを作成してもらう必要があります。

#### adminチームにアカウントを用意してもらう

チケットをDebian開発者に起票してもらうと、専用のトラッカー番号が割り当てられます。
これには、専用のメールアドレスがひもづけられています。例えば、トラッカー番号6092に対応して「rt+6092@rt.debian.org」といったような具合です。

しばらく待つと、DSAと呼ばれる管理者からアカウント作成完了通知がメールで届きます。
なかなか届かない場合は、専用のメールアドレスに対して現状どうなっているか確認のメールを投げるとよいでしょう。[^0]

申請通りにアカウントが作成されているかは次のコマンドで確認できます。[^1]

```
% ldapsearch -LLL -b dc=debian,dc=org -x -h db.debian.org uid=(申請したユーザー名) allowedHost
```


すると、次のようなレスポンスが返ってきます。

```
dn: uid=(申請したユーザー名),ou=users,dc=debian,dc=org
allowedHost: minkus.debian.org 20160427
allowedHost: etler.debian.org 20160427
```


ちなみに、この段階でDebian開発者でなくても(申請したユーザー名)@debian.orgが作成されますが、使ってはいけないことになっています。

#### ゲートウェイを経由してssh鍵を登録する

アカウントが作成できても、サーバーには公開鍵を使ってアクセスする必要があるので、鍵を登録しないといけません。
これには[LDAPゲートウェイ](https://db.debian.org/doc-mail.html)が用意されています。

例えば、minkusとetlerの申請をしている場合、公開鍵の先頭に以下を追加します。

```
allowed_hosts=minkus.debian.org,etler.debian.org ssh-rsa ....
```


そのあとで、`cat (公開鍵ファイル) | gpg --armor --sign`などとして署名したものをメールの本文に貼りつけて、changes@db.debian.org宛てで送付します。

#### アカウント情報が伝搬するまでしばらく待つ

即座に反映されるわけではないので、しばらく待ちましょう。
各サーバーに反映されると、sshでporterboxにログインできるようになります。

### まとめ

今回は、移植作業用のインフラをDebian開発者でない人が借りる方法について紹介しました。
実機をもってないけど、特定のアーキテクチャへの対応をしたいなら検討してみてはいかがでしょうか。[^2]

[^0]: 2週間くらい待ってみて、進捗どうですかとメールを投げてみたところ、すぐに対応してもらえたということがありました。

[^1]: ldapscriptsをあらかじめインストールしておきましょう。

[^2]: もちろんqemuを使うという選択肢もあります。
