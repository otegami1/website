---
tags:
- ruby
title: ActiveLdap 1.0.1リリース
---
LDAPのエントリを
[ActiveRecord](http://ar.rubyonrails.com/)風のAPIでア
クセスするためのライブラリ、
[ActiveLdap](http://rubyforge.org/projects/ruby-activeldap/)
1.0.1がリリースされました。
<!--more-->


### ActiveLdapとは

ActiveRecord風のAPIとは1エントリを1オブジェクトとして扱える
ということです。例えば、ユーザの説明を変更する場合は以下のよ
うになります。

{% raw %}
```ruby
alice = User.find("alice")
alice.description = "New user"
alice.save!
```
{% endraw %}

ActiveRecordと同じように、各クラス間の関係を設定して便利にア
クセスすることもできます。

{% raw %}
```ruby
class User < ActiveLdap::Base
  belongs_to :groups, :many => "memberUid"
end

class Group < ActiveLdap::Base
  has_many :users, :wrap => "memberUid"
end

alice = User.find("alice")
alice.groups # => [Group("friend"), Group("office"), ...]
alice.groups << Group.find("home")
alice.groups # => [Group("friend"), Group("office"), Group("home"), ...]

friend = Group.find("friend")
friend.users # => [User("alice"), User("bob"), ...]
```
{% endraw %}

ActiveRecordと同じように、[Ruby on
Rails](http://www.rubyonrails.org/)と使用することもでき
ます。

{% raw %}
```
% script/plugin install http://ruby-activeldap.googlecode.com/svn/tags/r1.0.1/rails/plugin/active_ldap
% script/generate scaffold_active_ldap
% vim config/ldap.yml
```
{% endraw %}

ActiveLdapは以下のライブラリをバックエンドとして利用できます。

  * [Ruby/LDAP](http://raa.ruby-lang.org/project/ruby-ldap/)
    * 拡張ライブラリ（速い、インストールが大変かもしれない）
  * [Net::LDAP](http://rubyforge.org/projects/net-ldap/)
    * Rubyのみで実装（遅い、インストールは簡単）
    * 2008/06/17時点の最新版0.0.4では動かない。
      [Subversion](http://net-ldap.rubyforge.org/svn/branches/experimental_netldap)
      版を利用する必要がある。
  * [JNDIのLDAPサービスプロバイダ](http://sdc.sun.co.jp/java/docs/j2se/1.4/ja/docs/ja/guide/jndi/jndi-ldap.html)
    （実験的）
    * JRubyでのみ利用可能。

### ベンチマーク

以下はActiveLdapに付属するベンチマークの結果です。ベンチマー
クでは100エントリを検索しています。「Rehearsal（リハーサル）」
を行って、それぞれ2回ずつ実行しているのは、以前はキャッシュ
などで2回目以降の結果がよくなることなどがあったためです。現
在はあまり意味がありませんが、歴史的に残っています。

{% raw %}
```
% ruby benchmark/bench-al.rb --config benchmark/config.yaml
Populating...
Rehearsal -------------------------------------------------------
  1x: AL              0.080000   0.010000   0.090000 (  0.098738)
  1x: AL(No Obj)      0.010000   0.000000   0.010000 (  0.016623)
  1x: LDAP            0.000000   0.000000   0.000000 (  0.008674)
  1x: Net::LDAP       0.030000   0.000000   0.030000 (  0.045199)
---------------------------------------------- total: 0.130000sec

                          user     system      total        real
  1x: AL              0.080000   0.020000   0.100000 (  0.100959)
  1x: AL(No Obj)      0.010000   0.010000   0.020000 (  0.020697)
  1x: LDAP            0.000000   0.000000   0.000000 (  0.010129)
  1x: Net::LDAP       0.030000   0.000000   0.030000 (  0.042075)
Entries processed by Ruby/ActiveLdap: 100
Entries processed by Ruby/ActiveLdap (without object creation): 100
Entries processed by Ruby/LDAP: 100
Entries processed by Net::LDAP: 100
Cleaning...
```
{% endraw %}

各項目はそれぞれ以下の通りです。

  * AL: Ruby/LDAPバックエンドのActiveLdapで検索を行い、各エ
    ントリをオブジェクト化する（ActiveRecord風のAPIを利用す
    る場合）
  * AL(No Obj): Ruby/LDAPバックエンドのActiveLdapで検索を行
    い、各エントリの結果をオブジェクト化しない（エントリを配
    列やハッシュなどを使って表現）
  * LDAP: Ruby/LDAPで検索を行う
  * Net::LDAP: Net::LDAPで検索を行う

上記の結果からは以下のことが言えます。

  * 本当に速度が重要な場合にはRuby/LDAPを直接利用する方がよ
    い。
  * 利用できるならば、Net::LDAPよりもRuby/LDAPバックエンドを
    利用した方がよい。
  * Net::LDAPを直接利用するよりも、オブジェクト化しない
    ActiveLdap + Ruby/LDAPバックエンドの方が速い。

多くの場合、1度に100エントリを処理することは少ないでしょう。
そのため、通常はActiveLdapで各エントリをオブジェクト化しても
問題は少ないといえます。

もし、1度に多くのエントリを扱う場合で、読み込み専用ならば、
オブジェクト化しない方法で利用することでパフォーマンスを改善
することができます。

### まとめ

ActiveLdapを利用することでLDAPのエントリをオブジェクト指向的
なAPIで自然に処理することができます。

ActiveLdapは複数のLDAPバックエンドに対応しており、Rubyがイン
ストールされている環境さえあれば動かすこともできます。
（Net::LDAPバックエンド使用時。ただしそんなに速くない）また、
JRubyでもほとんどの機能が動きます。

もし、Ruby/LDAPを利用できる環境であれば、Net::LDAPを直接利用
するよりも、ActiveLdap + Ruby/LDAPバックエンドを利用した方が
よりオブジェクト指向らしいAPIでLDAPのエントリを操作できます。
また、速度が要求される場合であれば、オブジェクト化を行わない
（オブジェクト指向らしいAPIを利用しない）ことにより、より高
速にLDAPのエントリを読み込むことができます。
