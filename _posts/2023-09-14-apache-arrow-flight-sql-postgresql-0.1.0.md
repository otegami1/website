---
author: kou
tags:
  - apache-arrow
title: "Apache Arrow Flight SQL adapter for PostgreSQL 0.1.0リリース！ #ApacheArrow #PostgreSQL"
---

2023年9月14日時点で[Apache Arrow](https://arrow.apache.org/)の[コミット数1位](https://github.com/apache/arrow/graphs/contributors)の須藤です。Apache Arrow本体の開発もがんばっているのですが、それとは別に、PostgreSQLにApache Arrow Flight SQLでアクセスできるようにするPostgreSQLの拡張機能も開発しています。昨日、その拡張機能[Apache Arrow Flight SQL adapter for PostgreSQL](https://arrow.apache.org/flight-sql-postgresql/current/)の最初のバージョン0.1.0をリリースしたので自慢します。

このプロダクトは[RubyKaigi 2023の発表]({% post_url 2023-05-15-rubykaigi-2023 %})の最後の方で少し自慢していたプロダクトです。

<!--more-->

## Apache Arrowとは？Apache Arrow Flight SQLとは？

長いプロダクト名の「Apache Arrow Flight SQL adapter for PostgreSQL」の中にわからない用語がいくつかあると思うので、まずは次の用語の説明をします。

* Apache Arrow
* Apache Arrow Flight SQL

[Apache Arrow](https://arrow.apache.org/)はメモリー上でのデータ処理に関わる共通基盤を提供するプロジェクトです。たとえば、[型付きの表形式のデータ用のフォーマット](https://arrow.apache.org/docs/format/Columnar.html)を定義しています。特定のプログラミング言語でだけ使えるように、ではなく、たくさんのプログラミング言語で使えるように開発が進められています。より多くの環境で使えたほうが共通基盤としてのメリットが大きくなるためです。Apache Arrowは共通基盤なのでユーザーが直接触れる機会はそう多くはありません。しかし、[pandas](https://pandas.pydata.org/)、[Polars](https://www.pola.rs/)、Google BigQuery Storageなどユーザーが直接触れるツールの裏で使われているので、すでに間接的に恩恵を受けているかもしれません。

[Apache Arrow Flight SQL](https://arrow.apache.org/docs/format/FlightSql.html)はApache Arrowが提供するネットワークプロトコルの1つです。このプロトコルはApache Arrowの各種機能をベースにして高速にSQLを実行するために設計されました。このプロトコルで注目している部分（最適化している部分）はSQLを実行するために必要なデータの転送部分です。`SELECT`しているときは選択されたデータをクライアントに返すところ、`INSERT`/`UPDATE`しているときはサーバーにデータを送るところになります。ここにApache Arrowが定義しているデータフォーマットを使うことで既存のプロトコルより高速になっています。

これらの用語がわかったところでプロダクト名「Apache Arrow Flight SQL adapter for PostgreSQL」をもう一度見てみましょう。この名前は説明的な名前になっていて、「PostgreSQL独自のプロトコルではなくApache Arrow Flight SQLというプロトコルでPostgreSQLを使えるようにして高速にPostgreSQLを活用しよう！」ということを狙ったプロダクトであることを示しています。

## 速いの？

速度が重要なプロダクトと言われたら本当に速いの？と思いますよね。私もそう思います。

将来的にはもっと網羅的に計測したいのですが、現時点では`integer`のカラム1つだけがあるテーブルに対する`SELECT *`で計測したデータだけがあります。

![ベンチマーク結果](https://raw.githubusercontent.com/apache/arrow-flight-sql-postgresql/main/benchmark/integer/result.svg)

注目して欲しい棒の色は青と緑です。オレンジと緑は実質同じ処理なので一方だけ注目すれば十分です。青がApache Arrow Flight SQLプロトコルでPostgreSQLにアクセスしたケースで、緑がPostgreSQL独自のプロトコルでPostgreSQLにアクセスしたケースです。青が今回開発したケースなので青ができるだけ速くあって欲しいです。

縦軸が実行時間なので短いほど速いです。

横軸は対象のレコード数です。1メモリあたり10倍になっているので対数スケールになっています。

この結果は対象データが大きくなるほど（右側の棒ほど）今回開発したプロダクトが有用であることを示しています。OLTP（OnLine Transaction Processing）のように少しのレコードの更新を大量にする処理や、PostgreSQL上で集計して少量のサマリー結果を返すような処理では有用ではありませんが、PostgreSQLから大量のデータを取得して別のデータ処理システムで処理するようなケースでは有用です。

この計測ではカバーできていませんが、`INSERT`/`UPDATE`でも同様の傾向になるはずなので、PostgreSQLに大量のデータを投入するケースでも有用なはずです。

この計測のために使ったスクリプトや結果などは[リポジトリー](https://github.com/apache/arrow-flight-sql-postgresql/tree/main/benchmark/integer)にあるので、詳細はそちらを参照してください。

## 機能

最初のリリースなのでまだいろいろ足りない機能がありますが、次の基本的な機能は実装済みです。

* preparedではない`SELECT`/`INSERT`/`UPDATE`/`DELETE`を実行する機能
* preparedな`SELECT`/`INSERT`/`UPDATE`/`DELETE`を実行する機能
* `password`/`trust`での認証
* TLSを使った接続
* 整数系の型のサポート
* 浮動小数点数系の型のサポート
* テキスト系の型のサポート
* バイナリー系の型のサポート
* タイムゾーンなしのタイムスタンプ型のサポート

## インストール方法

今のところDebian GNU/Linux bookwormとUbuntu 22.04用のパッケージのみ用意しています。

インストールするための具体的なコマンドラインは[オフィシャルサイトのインストールドキュメント](https://arrow.apache.org/flight-sql-postgresql/current/install.html)を参照してください。

PostgreSQL 15以降のみサポートしています。PostgreSQL 15以降にだけある機能を使っているからなのですが、ちょっとがんばればPostgreSQL 14以前もサポートできる気はしています。もし、古いPostgreSQLでも使いたい人がそれなりにいたらがんばるかもしれません。が、できれば新しいPostgreSQLを使って欲しい気持ちではいます。

## ロードマップ

今後は次のような改良をしていきたいと思っています。興味がある人は一緒に開発しましょう！[GitHubのissues](https://github.com/apache/arrow-flight-sql-postgresql/issues)や[Apache Arrowの`dev@arrow.apache.org`メーリングリスト](https://arrow.apache.org/community/)や[Red Data Toolsのチャット](https://app.element.io/#/room/#red-data-tools_ja:gitter.im)で待っています！

* サポートしているデータ型を増やす
* mTLSでの認証
* [メタデータを取得するApache Arrow Flight SQLコマンド](https://arrow.apache.org/docs/format/FlightSql.html#sql-metadata)のサポート
* Apache Arrow Flight SQLのトランザクション系のAPIのサポート
* クエリーキャンセルのサポート
* ベンチマークの追加
* 高速化（もっと速くできるはず！）
* アーキテクチャーのドキュメントを書く

## まとめ

PostgreSQLと大量のデータのやりとりするケースを高速にする[Apache Arrow Flight SQL adapter for PostgreSQL](https://arrow.apache.org/flight-sql-postgresql/current/)の最初のバージョン0.1.0をリリースしたことを自慢しました。

現時点で速くなるケースがあるので、PostgreSQLと大量のデータをやりとりするケースを高速にしたいんだよなぁと思っていた人は試してみてください。今は私が一人で開発している状態なので一緒に開発してくれる人が増えるとすごくうれしいです！

なお、この開発は[Voltron Dataさん](https://voltrondata.com/)が金銭的に支援してくれました。ありがとうございます。クリアコードに入って一緒に開発したい人は[採用情報]({% link recruitment/apache-arrow.md %})を参照してください。
