---
tags:
- presentation
title: DebConf18でuscanの改善に関する発表をしました
---
### はじめに

クリアコードの林です。
<!--more-->


今回は、[DebConf18](https://debconf18.debconf.org/)で発表する機会があったのでその内容を紹介します。
今年のDebConf18は台湾の国立交通大学（National Chiao Tung University - NCTU）にて開催されました。[^0]

![NCTU]({{ "/images/blog/20180806_1.jpg" | relative_url }} "NCTU")

桃園国際空港からはMRT[^1] とHSR[^2]、台湾バス[^3]を乗り継いで行きました。

![Taoyuan to Hsinchu]({{ "/images/blog/20180806_0.jpg" | relative_url }} "Taoyuan to Hsinchu")

DebConf18の[統計情報](https://debconf18.debconf.org/statistics/) [^4]によると、全体で309人が参加、そのうち日本からは27名が参加したようです。距離的にも近いということで参加者も多かったのではないでしょうか。

### Rethinking of debian/watch rule

![Xueshan Room X]({{ "/images/blog/20180806_2.jpg" | relative_url }} "Xueshan Room X")

  * [Rethinking of debian/watch rule - DebConf18の紹介ページ](https://debconf18.debconf.org/talks/95-rethinking-of-debianwatch-rule/)

発表資料はRabbit Slide Showにて公開しています。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kenhys/debconf18-rethinking-of-debian-watch/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kenhys/debconf18-rethinking-of-debian-watch/" title="Rethinking of debian/watch rule">Rethinking of debian/watch rule</a>
  </div>
</div>


Debianのパッケージでは、アップストリームの更新を検出するために `uscan` というプログラムが使われています。`uscan` の設定は `debian/watch` という設定ファイルに記述します。
`debian/watch` ファイルにはアップストリームのリリースファイルのアクセス先や、パッケージング内容に沿うようにファイル名を変換したりするなど、いくつかのルールを記述します。
このルールは正規表現を使って記述するのですが、場合によっては複雑なルールを記述しなければならないことがあります。

例えば、複雑なルールを記述しなければならない例の一つに、[OSDN.net](https://osdn.net/)でリリースされているソフトウェアがあります。

```
version=4
opts="uversionmangle=s/-beta/~beta/;s/-rc/~rc/;s/-preview/~preview/, \
pagemangle=s%<osdn:file url="([^<]*)</osdn:file>%<a href="$1">$1</a>%g, \
downloadurlmangle=s%projects/sawarabi-fonts/downloads%frs/redir\.php?m=iij&f=sawarabi-fonts%g;s/xz\//xz/" \
https://osdn.net/projects/sawarabi-fonts/releases/rss \
https://osdn.net/projects/sawarabi-fonts/downloads/.*/sawarabi-mincho@ANY_VERSION@@ARCHIVE_EXT@/ debian uupdate
```


Debianパッケージ化をするときには、必須ではありませんが `debian/watch` ファイルも用意してあると、その後のメンテナンスが楽になるのでおすすめです。
ただし、Debian Policyでは `Optional` 扱いです。[^5]

発表時には、パッケージングをするときに複雑になりがちな設定を簡単にできないかという観点で、`debian/watch` の使われ方の傾向を調べつつ、フォーマットの改善案を提案してみました。

発表して終わりという性質のものではないので、引き続き、bugs.debian.orgにて議論していければいいなぁと思っています。

  * [uscan: one proposal to v5 format](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=905329)

### おまけ

8/2の夜は海鮮が美味しいとされるおすすめのところでのカンファレンスディナーでした。[DebConfのスポンサー](https://debconf18.debconf.org/sponsors/)のおかげですね。

![カンファレンスディナーのパネル]({{ "/images/blog/20180806_5.jpg" | relative_url }} "カンファレンスディナーのパネル")

![画像の説明]({{ "/images/blog/20180806_3.jpg" | relative_url }} "画像の説明")
![画像の説明]({{ "/images/blog/20180806_4.jpg" | relative_url }} "画像の説明")
![画像の説明]({{ "/images/blog/20180806_6.jpg" | relative_url }} "画像の説明")

### まとめ

今回は、DebConf18に参加して発表する機会があったので、その内容を紹介してみました。

映像記録ありのセッションに関しては、順次[アーカイブ](https://meetings-archive.debian.net/pub/debian-meetings/2018/DebConf18/)からWebM形式の動画が参照できるようになっているようです。また、Youtubeの[DebConf Videosチャンネル](https://www.youtube.com/channel/UC7SbfAPZf8SMvAxp8t51qtQ)からも（過去のDebConfの映像を含め）閲覧することができるようになっています。
見逃したセッションがあればそちらを確認してみるのはいかがでしょうか。

[^0]: 博愛キャンパスと光復キャンパスとがありますが会場であるNCTU MIRC（National Chiao Tung University Microelectronics and Information Research Center）は光復キャンパスにあります。

[^1]: 桃園捷運（とうえんしょううん）。いわゆる地下鉄のこと。

[^2]: 台湾高速鉄道。いわゆる新幹線のこと。

[^3]: 前乗り、前降りというあまり日本人には馴染みのないスタイル。

[^4]: Debian SSOによるログインが必要です。

[^5]: https://www.debian.org/doc/debian-policy/ch-source.html#optional-upstream-source-location-debian-watch
