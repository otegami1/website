---
tags:
- fluentd
title: winevt_c gemを開発をした話
---
### はじめに

Windows EventLogをRubyで取得するには元々[win32-eventlog gem](https://github.com/chef/win32-eventlog)がありました。
このgemはWindows EventLogへ読み込みと書き込みができるものです。
ただし、文字コードに関してはCP_ACP（Windowsの初期コードページ）を使っていたため、日本語版WindowsではCP932（Windows-31J）の範囲内の文字列しか読み込むことができませんでした。
<!--more-->


### エンコーディングをより正確に扱うには？

Windows APIでは`char *`と`wchar *`を相互に変換するAPIを提供しています。

  * [WideCharToMultiByte](https://docs.microsoft.com/en-us/windows/win32/api/stringapiset/nf-stringapiset-widechartomultibyte) （`wchar *`から`char *`）

  * [MultiByteToWideChar](https://docs.microsoft.com/en-us/windows/win32/api/stringapiset/nf-stringapiset-multibytetowidechar)（`char *`から`wchar *`）

どの関数も先ず変換先の文字列を長さを取得してから再度取得した変換先の文字列の長さを用いて`char *` ↔ `wchar *`の変換を行います。

winevt_c gemでは、`wchar *`からRubyのUTF8の文字列を作成する関数を多用しました。

```cpp
VALUE
wstr_to_rb_str(UINT cp, const WCHAR *wstr, int clen)
{
    VALUE vstr;
    CHAR *ptr;
    int len = WideCharToMultiByte(cp, 0, wstr, clen, nullptr, 0, nullptr, nullptr);
    ptr = ALLOCV_N(CHAR, vstr, len);
    WideCharToMultiByte(cp, 0, wstr, clen, ptr, len, nullptr, nullptr);
    VALUE str = rb_utf8_str_new_cstr(ptr);
    ALLOCV_END(vstr);

    return str;
}
```


この関数は任意のコードページを指定できるようにしていますが、winevt_cでは基本的にCP_UTF8（UTF8のコードページ）を指定して呼び出しています。

```c
VALUE utf8str;
// some stuff.
utf8str = wstr_to_rb_str(CP_UTF8, <Wide Char Result>, -1);
```


### Windows EventLogの新しいAPI

Windows Vistaより、[Windows EventLogを読み取る新しいAPI](https://docs.microsoft.com/ja-jp/windows/win32/api/winevt/)が提供されています。
winevt_c gemではこのAPIを用いてRubyからWindows EventLogを読み込めるように実装しました。

### EvtFormatMessageを用いたEventLogのメッセージの取得方法

[EvtFormatMessage](https://docs.microsoft.com/en-us/windows/win32/api/winevt/nf-winevt-evtformatmessage)を用いると、EventLogのメッセージがWindowsの組み込みの管理ツールであるイベントビューアで取得できるメッセージと同じものが取得できます。winevt_cでは、[get_message関数内でEvtFormatMessageを使用しています](https://github.com/cosmo0920/winevt_c/blob/e7689d511295e070979f78dffc906e89bc1917f7/ext/winevt/winevt_utils.cpp#L315-L413)。このEvtFormatMessage関数の引数の`Flags`へ`EvtFormatMessageEvent`を指定しているところがポイントです。

### winevt_cの現状

winevt_cはfluent-plugin-windows-eventlogに新しく実装した[in_windows_eventlog2](https://github.com/fluent/fluent-plugin-windows-eventlog/blob/master/lib/fluent/plugin/in_windows_eventlog2.rb)のWindows EventLogを取得するメソッドを中心に実装しています。そのため、現状では以下の機能は実装されていません。

  * リモートにあるWindowsの認証情報を作成し、リモートのWindowsにあるWindows EventLogを取得する

  * 指定したWindows EventLogのチャンネルからevtxファイルとしてエクスポートする

  * チャンネルのメタデータや設定をRubyから取得する関数

### まとめ

fluent-plugin-windows-eventlogの[in_windows_eventlog2](https://github.com/fluent/fluent-plugin-windows-eventlog/blob/master/lib/fluent/plugin/in_windows_eventlog2.rb)を実装するにあたって作成したwinevt_c gemについて簡単に解説しました。この記事は[Fluentd meetup 2019でWindows EventLogに関するプラグイン回りの発表した話]({% post_url 2019-07-18-index %})では軽くしか触れられていないwinevt_cの役割と、現状を解説する目的で執筆しました。winevt_c gem単体ではWindows EventLogはXMLで取得され、中身を見るためにはXMLのパースが現状では必要になってしまいますが、RubyでWindows EventLogを扱う方法の一つとして検討してみてください。
