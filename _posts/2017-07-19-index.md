---
tags:
- groonga
- presentation
title: 'OSS on Azure 非公式コミュニティ #5 『Azure Database』勉強会：MySQLとPostgreSQLと日本語全文検索 - Azure
  DatabaseでMroonga・PGroongaを使いたいですよね！？  #AzureDB'
---
品川のマイクロソフトさんのオフィスに行ったのは[RubyHiroba 2013](http://rubyhiroba.org/2013/)以来な気がする須藤です。
<!--more-->


[db tech showcase OSS 2017](http://www.db-tech-showcase.com/dbts/OSS)の懇親会で[Azure Database for MySQL / PostgreSQL](https://www.slideshare.net/InsightTechnology/dbtsoss2017-partysesseion-microsoft)というセッションがありました。Azure Databaseで[Mroonga](http://mroonga.org/ja/)・[PGroonga](https://pgroonga.github.io/ja/)を使えるようになったりしませんか！？と聞いてみたところ、「月末にAzure Databaseを開発している人がくるAzure Databaseのイベント[OSS on Azure 非公式コミュニティ #5 『Azure Database』勉強会](https://ossonazure.connpass.com/event/59939/)があるので、そこでMroonga・PGroongaを紹介すると話が進むかも！？」ということだったので紹介してきました。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/oss-on-azure-5/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/oss-on-azure-5/" title="MySQLとPostgreSQLと日本語全文検索 - Azure DatabaseでMroonga・PGroongaを使いたいですよね！？">MySQLとPostgreSQLと日本語全文検索 - Azure DatabaseでMroonga・PGroongaを使いたいですよね！？</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/oss-on-azure-5/)

  * [スライド（SlideShare）](https://www.slideshare.net/kou/ossonazure5)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-oss-on-azure-5)

### 内容

この発表では次のことを紹介しました。

  * Mroonga・PGroongaが高速なこと

  * Mroonga・PGroongaはMySQL・PostgreSQLユーザーなら簡単に使えること

  * Mroonga・PGroongaはWindowsでも動くこと

  * PGroongaではJSON内のテキスト全部を全文検索できること

  * PGroongaでは簡単に入力補完を実現できること

話の最初と最後でAzure DatabaseでMroonga・PGroongaを使いたい人はどのくらいいるか聞いてみました。最初は1割くらいでしたが最後は半分くらいの人が使いたい気持ちになっていました。Azure Databaseの開発をしている方は持ち帰ってチームで検討すると言っていました。Azure Database for MySQL/PostgreSQLは今はまだプレビュー期間中ですが、一般公開時にはMroonga・PGroongaをサポートしているかもしれません。Azure Database for MySQL/PostgreSQLでMroonga・PGroongaを使いたい方はぜひAzure Databaseのサポートに「Mroonga・PGroongaを使いたい！」とフィードバックしてください。

### まとめ

Azure Databaseが[MySQL](https://azure.microsoft.com/ja-jp/services/mysql/)と[PostgreSQL](https://azure.microsoft.com/ja-jp/services/postgresql/)をサポートしようとしています。どちらも現状では日本語全文検索が苦手ですが、Azure DatabaseがMroonga・PGroongaをサポートすれば日本語全文検索が得意なDBaaSになります。まだMroonga・PGroongaをサポートすることになったわけではないので、Azure DatabaseでMroonga・PGroongaを使いたい方はサポートにフィードバックしてください。

### おしらせ

8月1日（火）14:00-16:00に[MySQL・PostgreSQL上で動かす全文検索エンジン「Groonga」セミナー](https://groonga.doorkeeper.jp/events/62741)を開催します。Mroonga・PGroongaを検討しているという方はぜひこの機会をご活用ください！
