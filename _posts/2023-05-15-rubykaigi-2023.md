---
tags:
- ruby
- presentation
title: "RubyKaigi 2023 - Ruby + ADBC - A single API between Ruby and DBs #rubykaigi"
author: kou
---

[RubyKaigi 2023](https://rubykaigi.org/2023/)で[Ruby + ADBC - A single API between Ruby and DBs](https://rubykaigi.org/2023/presentations/ktou.html)という[ADBC](https://arrow.apache.org/adbc/)の話をしてきた須藤です。

なお、[クリアコードはシルバースポンサーとしてRubyKaigi 2023を応援](https://rubykaigi.org/2023/sponsors/#sponsor-436)しました。

<!--more-->

<div class="youtube">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/vwEiRTK32Ik" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

<div class="rabbit-slide rabbit-slide-wide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/rubykaigi-2023/viewer.html"
          width="640" height="404"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/rubykaigi-2023/" title="Ruby + ADBC - A single API between Ruby and DBs">Ruby + ADBC - A single API between Ruby and DBs</a>
  </div>
</div>

関連リンク：

  * [動画（YouTube）](https://www.youtube.com/watch?v=vwEiRTK32Ik)

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/rubykaigi-2023/)

  * [リポジトリー](https://gitlab.com/ktou/rabbit-slide-kou-rubykaigi-2023)

### 内容

話した内容は前述のスライドと[事前情報記事]({% post_url 2023-05-08-rubykaigi-2023-announce %})を参考にしてください。しばらくしたら話したときの動画が公開されると思うので公開されたらリンクを追加しておきます。

### よかった話

今年は[咳さんの話](https://speakerdeck.com/m_seki/learn-ractor)がよかったです。特に「Concurrency is everywhere」と「Sequential execution is a special case」がよかったです。咳さんは多くの人が「ふつう」だと思っていそうなことに「それは違うんじゃない？」という話をすることが多々あって（多々あるように私には見えていて）、今年のこの視点もさすがだなぁと思ったのでした。さすがだなぁと思いすぎて私の話の中でも咳さんの話を紹介してしまいました。

### JRuby + Apache Arrow

JRubyの開発者である[@headius](https://twitter.com/headius)がJRubyからもADBCを使えるようにするため、[JRubyのApache Arrow対応を提案](https://github.com/apache/arrow/issues/35589)しました。CRuby用のApache ArrowバインディングはC++実装のバインディングですが、JRubyならJava実装でしょ！ということで、Java実装のバインディングを使う方向で検討しています。

RubyKaigiで話すとこんな感じで開発がブーストすることが多いのでとてもいい機会ですね！みんなも自分が書いているコードの話をするといいよ！

### 研鑽Rubyプログラミング

先日[研鑽Rubyプログラミングの感想をまとめ]({% post_url 2023-04-27-polished-ruby-programming %})ていたのは、研鑽Rubyプログラミングの著者の[@jeremyevans0](https://twitter.com/jeremyevans0)が[RubyKaigi 2023で話す](https://code.jeremyevans.net/presentations/rubykaigi2023/)からだったのです！

@jeremyevans0と訳者の[@kakutani](https://twitter.com/kakutani)からサインをもらいました。[@jeremyevans0とは記念撮影](https://twitter.com/ktou/status/1656872167336992770)もしました。記念撮影のあと、一緒にお昼ご飯を食べに行きました。どうやって箸でお寿司を食べるかを教えました。（私は素手ではなくて箸でお寿司を食べる派です。）

お昼ご飯のあとはサイン会があるということだったので、[ふるさと](https://taiyaki-ya.com/)の天然たいやきを買って届けました。これは[たいやき部](https://taiyaki.club/)の部長としての活動です。

### Rabbit

いつもRabbitを使っている人たちが今年もRabbitを使っていました。RabbitユーザーにはRabbitユーザーの証である「ショッカーマニア」[^shocker-mania]の人形を渡しているのですが、この数年は持っていくのを忘れていて渡していませんでした。しかし！今年は忘れずに持っていったので[@hasumikin](https://twitter.com/hasumikin/)と[@yu_suke1994](https://twitter.com/yu_suke1994)と[@heronshoes](https://twitter.com/heronshoes)に渡しました。

[^shocker-mania]: 今はもう販売していません。「ポピー ショッカーマニア」で検索すると情報が見つかるかもしれません。

[@hasumikinはイベントごとにテーマを作っている](https://slide.rabbit-shocker.org/authors/hasumikin/)（RubyKaigi 2023用のテーマとか）のですが、同じイベントで話す他のRabbitユーザーも使いやすいように https://github.com/rabbit-shocker/rabbit に直接pushしてもらうことにしました。（コミット権の設定をしました。）Rabbitのテーマはgemとして配布できるのでその仕組みを使うこともできますが、通常はイベントでの話が終わったあとや話す少し前に`gem push`するので共有されるタイミングが遅くなってしまいます。Rabbitのリポジトリーに入れてしまえば最新のRabbitを使うだけで共有できるのでそうすることにしました。

@yu_suke1994にコードのハイライトバックエンドはカスタマイズできると伝えてしまいましたがウソでした。。。デフォルトでRougeが使われていて変更はできませんでした。。。

@heronshoesは[別のツールで生成したPDF](https://speakerdeck.com/heronshoes/the-adventure-of-redamber-a-data-frame-library-in-ruby)をRabbitで表示していました。（Rabbitは[昔からPDFビューアーでもある]({% post_url 2009-08-31-index %})のです！）が、PDFビューアーとして使うときにテーマをカスタマイズすることが面倒なことがわかったので、さっき少し改良しておきました。

まつもとさんのスライドにうさぎとかめが表示されていなかったのはまつもとさんがうさぎとかめのお世話を怠って逃げられたわけではありませんでした。新しいmacOS上でRabbitが安定していないことが原因でした。まつもとさんは普段はLinuxマシンを使っているそうですが、そのLinuxマシンの電源アダプターを忘れてしまってMacマシンを使わないといけなくなったそうです。しかし、MacマシンでRabbitが動なかったためRabbitでPDF化したスライドをMacマシンで表示していたとのことでした。

RabbitをGTK 3ではなくGTK 4で動くようにすれば新しいmacOS上でもいい感じに動く可能性があるので、まつもとさんが次に話をする機会がある10月前までにRabbitをGTK 4に対応させてリリースしたいなぁという気持ちになりました。gtk4 gemはすでにできているので主にRabbitを変更するだけでいけるはず。

### Red Data Tools

今年はRed Data Toolsからは私と@heronshoesが話しました。ここ数年Red Data Toolsの開発イベントはオンライン開催でRed Data Toolsのみんなと会っていないので、RubyKaigi 2023初日の夜に[Red Data Toolsのみんなで集まりました](https://red-data-tools.connpass.com/event/283551/)。いつものRed Data Toolsのみんなだけではなく、Official Partyで誘った新しい人たちもいました。

新しい人たちは[Rubyのパーサージェネレーターを置き換えた](https://speakerdeck.com/yui_knk/the-future-vision-of-ruby-parser)[@spikeolaf](https://twitter.com/spikeolaf)と一緒に開発をしたい人たちでした。@spikeolafがやっていることを一緒にやるにはC言語の知識があった方がよさそうということでした。しかし、その人たちはまだC言語の知識はあまりありませんでした。ということで、Red Data ToolsでC言語まわりの経験を積んでから@spikeolafと合流しようということになったのです。

新しい人のうちの1人は、私が行きの電車の中でついつい声をかけてしまった人でした。今年はクリアコードから2人で参加する予定で2人分の隣り合った席を予約していたのですが、体調不良で私しかいけなくなりました。そのため、前日に私の隣の席をキャンセルしました。前日にキャンセルしたのでだれも座らないだろうと思ったのですが、途中から横に座った人がいました。私は寝ていたのですが、目が覚めてふと横を見るとノートパソコンでRubyKaigi 2023のタイムテーブルを見ているではありませんか。しかも、なにやら各トークを予習しているようです。どうしようかと思いましたが、今年は[RubyKaigi 2023 予習イベント ～推しトーク紹介～](https://sms-tech.connpass.com/event/281969/)でトーク紹介をしていて私は結構詳しい方なはずなので、ついつい「私、スピーカーですがタイムテーブルを解説しましょうか？」と声をかけてしまいました。電車でのタイムテーブルを終えて別れましたが、Official Partyで再開しました。どの話が面白かったかを聞いたら@spikeolafの話というのでちょうど近くを通った@spikeolafを捕まえて一緒に開発しなよ！とけしかけました。が、C言語の知識があった方がよさそうということになったのでまずはRed Data Toolsで経験を積むことになりました。

そんな[@mterada1228](https://twitter.com/mterada1228)は「[@TKStefano](https://twitter.com/TKStefano)と一緒に毎週水曜日の12:15-12:45にRed Arrowを題材にRubyの拡張ライブラリーをする配信」に混ざることになりました。興味のある人は[Red Data Tools関連動画プレイリスト](https://www.youtube.com/playlist?list=PLKb0MEIU7gvQOIACKgdgKuAE7cMPDuTE6)の新しい方の動画をチェックしたりウォッチしてみてね。

### RubyGems

私はバインディングを書くことも多いです。バインディングをインストールするには、バインディングそのものだけではなく、バインディング対象のライブラリーもインストールしなければいけません。私はシステムのバインディング対象ライブラリーを使いたい派ですが、そうではなく、gemにバインディング対象のライブラリーも同梱したい派の人たちがいます。

[2019年、fat gemをやめる]({% post_url 2019-11-22-index %})で説明している通り、私はシステムのバインディング対象ライブラリーを自動でインストールできるようにしています。システムが適切に設定されていれば途中でユーザーに明示的な確認を求めずにすべていい感じにインストールします。多くのユーザーはバインディング対象のライブラリーが必要なら自動でインストールして欲しいはず（「インストールしていいですか？」とプロンプトがでたら「yes」と返すはず）なので、よほどのことがない限り確認がない方が便利だと私は思っています。しかし、そうではない人もいます。

なので、そうではない人もいい感じになにかできるような仕組みをRubyGemsレベルで整備するかー、とRubyKaigi 2022（去年！）の頃に考えていて、メンテナンスをしている[@hsbt](https://twitter.com/hsbt)とこんな感じでどう？と相談していました。が、「じゃあ、こういう感じで！」というやつの実装をRubyKaigi 2022の帰りに終わらせられませんでした。そのため、「あぁ、後で仕上げないとなぁ」と思いながらRubyKaigi 2023になってしまいました。RubyKaigi 2023でそれを思い出したので仕上げました。

詳細はまた別の機会に説明しますが、[RubyGemsのプラグインがインストール直後からすぐに有効になるようにして](https://github.com/rubygems/rubygems/pull/6673)、RubyGemsのプラグインとしてシステムのバインディング対象のライブラリーをいい感じにインストールするやつを用意しようと思っています。

### test-unit

@hsbtにGitHub Actionsでtest-unitの実行結果がコンパクトになると見通しがよくなってうれしいと言われたので`GITHUB_ACTIONS=true`ならデフォルトでいい感じの出力になるようにしておきました。デフォルトでいい感じになるので新しいバージョンのtest-unitにするだけでいい感じになります。

### OSS Gate

OSS Gateをもっといい感じにしたいと[@yasulab](https://twitter.com/yasulab)に相談したら、[Railsチュートリアルを読んだあとの次のステップの1つとして紹介](https://twitter.com/yasulab/status/1657775270810767361)してくれたり、[フィヨルドブートキャンプ](https://bootcamp.fjord.jp/)をやっている[@machida](https://twitter.com/machida)を呼び出してくれたりしました。OSS Gateとフィヨルドブートキャンプでもコラボできそうなので楽しみです！

### 若者のOSSとの関わり方

私はSpeeeさんのOSS活動をサポートしています。今年は入社2年目の若者である[@takatea_dayo](https://twitter.com/takatea_dayo)がRubyKaigiにオフライン参加するということだったので、RubyKaigiにいる野生のOSS開発者たちがどういう風にOSS活動をしているかを知って欲しいなぁと思いました。たまたま歩いていた[@a_matsuda](https://twitter.com/a_matsuda)とたまたま歩いていた[@onk](https://twitter.com/onk)に自分のことを話してもらえました。いいきっかけになっていたらいいなぁと思います。

### まとめ

RubyKaigi 2023では、話をしたり、スポンサーをしたり、RabbitとかRubyGemsとかtest-unitとかをいじったり、OSSのことをしたりしていました。

今年もいいRubyKaigiでした！オフライン参加してよかった！
