---
tags: []
title: Firefox Quantum以降のglobalChrome.cssの移行について
---
### はじめに

企業利用においては、Firefox/Thunderbirdの特定の機能を使わせないようにカスタマイズしたいという場合があります。
そのための機能を提供するアドオンに`globalChrome.css`があります。
（使用例は過去の[Thunderbirdのインターフェースをカスタマイズするには]({% post_url 2017-11-13-index %})という記事をご参照下さい。）
<!--more-->


しかし、WebExtensions APIへの移行の流れにともない、Firefox ESR59以降では`globalChrome.css`は使えないことが決まっています。また、Firefox Quantum以降ではすでにアドオンの互換性がなくなっています。

今回は、`globalChrome.css`で行っていたカスタマイズを別の手段で代替する方法と、その注意点をご紹介します。

### globalChrome.cssとは

  * [globalChrome.css](https://addons.mozilla.org/ja/firefox/addon/globalchromecss/)

Firefox/Thunderbirdは、アプリケーション自体のUIがXMLとCSSで構成されています。
このアドオンは、Firefox/ThunderbirdのUIに追加のスタイルシートを反映することで、その外観を変化させるというアドオンです。
冒頭で紹介したように、企業利用においては不要な機能をあらかじめ削除するなど、ユーザーインターフェースをその企業に合わせてカスタマイズした状態で利用したい場合に使われます。

しかしながら、前述の通りこのアドオンはFirefox Quantum以降では動作しません。
これを使ってFirefox/Thunderbirdをカスタマイズしている場合、代替手段が必要です。
それがユーザースタイルシートと呼ばれるものです。[^0]

ユーザースタイルシートには次の2つがあります。

  * userChrome.css

  * userContent.css

`globalChrome.css`はこのうち`userChrome.css`にて代替することができます。

### userChrome.cssとuserContent.cssの違い

ユーザースタイルシートといわれるのがこれら2つのファイルですが、違いはなんでしょうか。
[Mozilla のカスタマイズ](https://www.mozilla-japan.org/unix/customizing.html)には、次のような説明があります。

  * userChrome.cssは「Mozilla アプリケーションの UI クロームに関するCSSを管理」

  * userContent.cssは「ウィンドウ内のコンテンツに関するCSSを管理」

これらのファイルの違いをuserChrome.cssとuserContent.cssのサンプル設定から示します。

userChrome.cssには文字色を赤にする全称セレクタの設定を適用してみます。

```
*|* {
    color: red !important;
}
```


userContent.cssには文字色を青にする全称セレクタの設定を適用してみます。

```
*|* {
    color: blue !important;
}
```


すると、UIクローム部分は赤い文字に、コンテンツ領域は青い文字になります。
この事から、それぞれのユーザースタイルシートがFirefox/Thunderbirdのどの部分に作用するかが分かります。

![どの部分に作用するかを示すスクリーンショット]({{ "/images/blog/20171208_0.png" | relative_url }} "どの部分に作用するかを示すスクリーンショット")

### globalChrome.cssからuserChrome.cssに移行するポイント

`globalChrome.css`は`userChrome.css`にて代替することができると述べましたが、両者には1点大きな違いがあります。展開のタイミングです。

`globalChrome.css`は通常カスタマイズ済みFirefoxのインストーラとともにバンドルされます。そのためインストール時に所定の場所に配置されます。
しかし、`userChrome.css`はユーザーのプロファイルディレクトリに配置しなければいけません。
プロファイルが作成されるのは、インストール後の起動時ですので配置すべきタイミングが異なります。

css  | 配置先(Windowsの場合)
--------|----------
globalChrome.css |C:\Program Files (x86)\Mozilla Firefox\chrome\globalChrome.css
userChrome.css| %APPDATA%\Mozilla\Firefox\Profiles(プロファイルフォルダ)\chrome\userChrome.css

したがって、プロファイル作成後に`userChrome.css`を配置する仕組みを構築する必要があります。[^1]

### まとめ

今回は、`globalChrome.css`が来たるべきFirefox ESR59以降では使えなくなることにともない、その移行のポイントについて紹介しました。

FirefoxやThunderbirdの導入やカスタマイズでお困りで、自力での解決が難しいという場合には、[有償サポート窓口](/contact/)までぜひ一度ご相談下さい。

[^0]: アドオンの互換性に問題がでるため用意された新たな仕組みではなく、ユーザースタイルシート自体は昔からあるものです。

[^1]: プロファイルフォルダに特定のファイルをコピーする必要があった事例に関して、autoconfig.cfgにて対応したケースが過去にあります。
