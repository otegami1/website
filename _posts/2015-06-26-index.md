---
tags:
- groonga
title: Groongaがキャッシュしている内容を確認する方法
---
### はじめに

Groongaには[cache_limit](http://groonga.org/ja/docs/reference/commands/cache_limit.html)というコマンドがあります。
`cache_limit` を使うと、最新のN件の `select` コマンドの結果をキャッシュさせることができます。
<!--more-->


しかし、実行した `select` コマンドをキャッシュしたのかどうかや、今どんな内容がキャッシュされているかについてはわかりません。
そこで、今回はプラグインを導入することで、簡単にGroongaのキャッシュの中身を覗く方法を紹介します。

### プラグインをインストールしよう

`/usr/local` 以下にGroongaをインストールしてあるという前提で説明します。

```sh
% git clone https://github.com/kenhys/groonga-plugin-grncache.git
% cd groonga-plugin-grncache.git
% ./autogen.sh
% ./configure --with-groonga-source-dir=PATH_TO_GROONGA_SOURCE_DIRECTORY
% make
% sudo make install
```


`PATH_TO_GROONGA_SOURCE_DIRECTORY` にはソースアーカイブのパスを指定します。[^0]

この記事を書いている時点では、Groonga 5.0.4が最新ですので、もし `/tmp` 以下にソースアーカイブを展開しているなら、`configure` の指定は次のようにします。

```sh
% ./configure --with-groonga-source-dir=/tmp/groonga-5.0.4
```


インストールまで完了すると、`/usr/local/lib/groonga/plugins/grncache/grncache.so` が存在するはずです。

古いバージョンのGroongaと一緒に試す場合、Groonga 3.0.8[^1]以降であればこのプラグインが動作するはずです。この制限はGrncacheが使用しているAPIが3.0.8以降に追加されたためです。

### キャッシュの中身を覗いてみよう

#### プラグインを登録する

実際にキャッシュの中身を覗いてみるまえに、プラグインを登録しておきましょう。

プラグインの登録には `plugin_register` コマンド [^2] を実行します。 Groongaを対話的に起動している場合は次のようにします。

```sh
> plugin_register grncache/grncache
```


HTTPサーバーとして起動している状態なら、次のようにします。

```sh
% curl http://localhost:10041/d/plugin_register?name=grncache/grncache
```


#### 動作確認用のデータを登録する

動作を確認するのに、サンプルとなるデータを登録しておきましょう。次のコマンドをGroongaの対話モードで実行します。

```sh
> table_create Site TABLE_HASH_KEY ShortText
> column_create --table Site --name title --type ShortText
> load --table Site
[
{"_key":"http://example.org/","title":"This is test record 1!"},
{"_key":"http://example.net/","title":"test record 2."},
{"_key":"http://example.com/","title":"test test record three."},
{"_key":"http://example.net/afr","title":"test record four."},
{"_key":"http://example.org/aba","title":"test test test record five."},
{"_key":"http://example.com/rab","title":"test test test test record six."},
{"_key":"http://example.net/atv","title":"test test test record seven."},
{"_key":"http://example.org/gat","title":"test test record eight."},
{"_key":"http://example.com/vdw","title":"test test record nine."},
]
```


#### キャッシュされたかを確認する

キャッシュの状態を確認するには、`grncache status` を実行します。

```sh
> grncache status
[[0,1435303725.13405,0.000181674957275391],{"cache_entries":0,"max_cache_entries":100,"cache_fetched":0,"cache_hit":0,"cache_hit_rate":0.0}]
```


それぞれの項目の内容は次の通りです。

|項目|説明|
|----|----|
|`cache_entries`|キャッシュされた検索結果の数|
|`max_cache_entries`|キャッシュ可能な検索結果の最大数。`cache_limit` で変更できる|
|`cache_fetched`|キャッシュを参照した回数。`select` を実行するたびに増加する|
|`cache_hit`|キャッシュにヒットした回数|
|`cache_hit_rate`|キャッシュにヒットした割合 (`cache_hit`/`cache_fetched`で算出される)|

初期状態なので `max_cache_entries`を除いてすべて0です。

キャッシュの中身をダンプすることもできます。それには、`grncache dump` を実行します。初期状態なのでこちらも0件です。

```sh
> grncache dump
[[0,1435303923.9083,8.46385955810547e-05],[[0]]]
```


では、`select` コマンドを実行してみましょう。

```sh
> select Site
[[0,1435303997.02583,0.000574111938476562],[[[9],[["_id","UInt32"],["_key","ShortText"],["title","ShortText"]],[1,"http://example.org/","This is test record 1!"],[2,"http://example.net/","test record 2."],[3,"http://example.com/","test test record three."],[4,"http://example.net/afr","test record four."],[5,"http://example.org/aba","test test test record five."],[6,"http://example.com/rab","test test test test record six."],[7,"http://example.net/atv","test test test record seven."],[8,"http://example.org/gat","test test record eight."],[9,"http://example.com/vdw","test test record nine."]]]]
```


`grncache status` を実行してみると、`cache_entries` が 1に増え、`select Site` の結果がキャッシュされたことがわかります。

```sh
> grncache status
[[0,1435304001.04143,0.000191450119018555],{"cache_entries":1,"max_cache_entries":100,"cache_fetched":1,"cache_hit":0,"cache_hit_rate":0.0}]
```


キャッシュの中身をダンプしてみましょう。

```
> grncache dump
[[0,1435304009.74493,0.000130653381347656],[[1],[{"grn_id":1,"nref":0,"timeval":"2015-06-26 16:33:17.025827","value":"[[[9],[[\"_id\",\"UInt32\"],[\"_key\",\"ShortText\"],[\"title\",\"ShortText\"]],[1,\"http://example.org/\",\"This is test record 1!\"],[2,\"http://example.net/\",\"test record 2.\"],[3,\"http://example.com/\",\"test test record three.\"],[4,\"http://example.net/afr\",\"test record four.\"],[5,\"http://example.org/aba\",\"test test test record five.\"],[6,\"http://example.com/rab\",\"test test test test record six.\"],[7,\"http://example.net/atv\",\"test test test record seven.\"],[8,\"http://example.org/gat\",\"test test record eight.\"],[9,\"http://example.com/vdw\",\"test test record nine.\"]]]"}]]
```


`[[1], [{"grn_id":1,"nref":0,"timeval":...,"value":...` という箇所があるのがわかります。
`[1]` というのがキャッシュエントリの数です。 `"value":...` というのがキャッシュされた検索結果です。先程実行した `select Site` と結果が一致していることがわかります。

もう一度同じ検索をした後だとどうなるでしょうか。

```sh
> grncache status
[[0,1435304396.55938,0.000149011611938477],,{"cache_entries":1,"max_cache_entries":100,"cache_fetched":2,"cache_hit":1,"cache_hit_rate":50.0}]
```


予想通り、`cache_fetched` と `cache_hit` が増え、キャッシュヒット率が 50% となりました。同じ検索なので、キャッシュ済みの内容をそのまま返してきたことがわかります。

### まとめ

Groongaがキャッシュしている内容をプラグインを使って確認する方法を紹介しました。
今どんな内容がGroongaによってキャッシュされているのかを確認したいときには参考にしてみてください。

[^0]: GrncacheはGroongaの内部構造に強く依存しているため、ソースコードを参照する必要があります。

[^1]: ちなみにGroonga 3.0.8がリリースされたのは、2013年9月29日のこと。

[^2]: Groonga 5.0.0以前の場合には register コマンド
