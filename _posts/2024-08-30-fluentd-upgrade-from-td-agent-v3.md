---
title: "Treasure Agent (td-agent) v3からFluent Package v5へのアップグレード方法"
author: kenhys
tags:
- fluentd
---

Treasure Agent (td-agent)はログ収集ソフトウェアであるFluentdに各種プラグインを同梱したパッケージです。
deb、rpmだけでなくWindows向けのパッケージも提供していたことから広く使われています。

Treasure Agent (td-agent)には後継となるパッケージがあり、、それがFluent Packageです。
通常版だけでなく、長期サポートを提供するためのLTS版の2つがあります。
本記事を執筆時点の通常版の最新は[v5.1.0](https://www.fluentd.org/blog/fluent-package-v5.1.0-has-been-released)であり、LTS版の最新は[v5.0.4](https://www.fluentd.org/blog/fluent-package-v5.0.4-has-been-released)がリリースされています。

Treasure Agent (td-agent)は、後継であるFluent Packageのリリース後しばらくして2023年12月に[サポートを終了](https://www.fluentd.org/blog/schedule-for-td-agent-4-eol)しました。
しかしながら、なかなかバージョンアップに踏み切れていないままだったお客様からの問い合わせが発生することがあります。

今回は、Treasure Agent (td-agent) v3系から最新のFluent Package v5 (LTS版)へのアップグレード方法と注意点について紹介します。

<!--more-->

## Treasure Agent (td-agent) v3やTreasure Agent (td-agent) v4からのアップグレードについて

Fluent PackageはTreasure Agent (td-agent)の後継として開発されたパッケージという背景もあり、Treasure Agent (td-agent) v4からのアップグレードをサポートしています。

しかし、当時すでにサポートを終了していたTreasure Agent (td-agent) v3からの直接のアップグレードはサポートしていません。
したがって、Treasure Agent (td-agent) v3からFluent Package v5にアップグレードするには、Treasure Agent (td-agent) v4を経由してからアップグレードする必要があります。

Treasure Agent (td-agent) v3から直接Fluent Packageにアップグレードしようと考える方もいるかとは思いますが、ディレクトリ構成等の違いによりFluentdが起動しなくなります。v3からの直接アップグレードは基本的にすべきではありません。[^direct-upgrade-cause-hazard]

[^direct-upgrade-cause-hazard]: Treasure Agent (td-agent) v3が想定するディレクトリ構成と、Fluent Package v5が想定しているディレクトリ構成の違いにより、マイグレーション処理が期待通りに動作しないことがわかっています。Treasure Agent (td-agent) v4へのアップグレードを挟まないため短縮できるメリットがあると思うかもしれませんが、Fluent Packageについて熟知していて発生したトラブルに対処できる、というわけではなければやってはいけません。

## Treasure Agent (td-agent) v4を経由したアップグレード手順

Treasure Agent (td-agent) v3からFluent Package v5への推奨するアップグレード手順(RPMの場合)は次のとおりです。

1. Treasure Agent (td-agent) v3のサービスを停止する
2. [旧インストール手順のドキュメント](https://docs.fluentd.org/installation/obsolete-installation/treasure-agent-v4-installation/install-by-rpm-td-agent-v4)を参照してv4をインストールする

   ```bash
   curl -L https://toolbelt.treasuredata.com/sh/install-redhat-td-agent4.sh | sh
   ```

3. [v5のインストール手順のドキュメント](https://docs.fluentd.org/installation/install-by-rpm)を参照して、fluent-package v5 (LTS版)をインストールする
  
    ```bash
    curl -fsSL https://toolbelt.treasuredata.com/sh/install-redhat-fluent-package5-lts.sh | sh
    ```

4. v3で追加でインストールしていたプラグインがあれば、それらをインストールしなおす
5. サービスを明示的に起動する

   ```bash
   sudo systemctl start fluentd
   ```

Treasure Agent (td-agent) v3からFluent Package v5ではRubyのバージョンが2.4系から3.2系へと刷新されています。
そのため、場合によってはそれまで使用していたプラグインが使えなくなっていることもあるかもしれません。
検証環境においてアップグレードの事前検証を推奨します。

<div class="callout primary">
Treasure Agent (td-agent) v4からFluent Package v5への更新については、次のような各種資料や動画が公開されているので参考にしてみてください。

* 公式ブログ(英語): <a href="https://www.fluentd.org/blog/upgrade-td-agent-v4-to-v5">Upgrade to fluent-package v5</a> 
* 弊社ブログ: [fluent-package v5への更新【Fluentd.org記事翻訳】]({% post_url 2024-01-24-upgrade-to-fluent-package-v5-from-v4 %})
* 弊社提供資料: [Fluent Package LTS（長期サポート版Fluentdパッケージ）ガイド]({% link downloads/fluent-package-lts-guide.md %})
* 動画解説: [Fluentdを現在ご利用の方向け　How to LTS移行【切り抜き】動画](https://www.youtube.com/watch?v=e5Lfnze092A)

例えば、Windows特有の注意事項なども動画解説では触れています。
</div>

## Treasure Agent (td-agent) v4を経由しない直接アップグレード手順

<div class="callout alert">
通常推奨される手順ではありません。どのようなマイグレーションが必要になるかという参考程度の情報として示しています。
</div>

* v3をアンインストール
  * 例: `sudo yum remove -y td-agent`
* 旧設定ディレクトリーを削除、または退避
  * 削除例: `sudo rm -rf /etc/td-agent`
  * 退避例: `sudo mv /etc/td-agent /tmp/td-agent`
  * v5インストール後、設定ファイル群の再配置を実施する
* 旧ログをディレクトリーを削除、または退避
  * 削除例: `sudo rm -rf /var/log/td-agent`
  * 退避例: `sudo mv /var/log/td-agent /tmp/td-agent-log`
* v5の新規インストール
  * 例: `curl -fsSL https://toolbelt.treasuredata.com/sh/install-redhat-fluent-package5-lts.sh | sh`

* 互換性のためのシムリンク作成
  * /etc/td-agent -> /etc/fluent
    * 例: `sudo ln -sf /etc/fluent /etc/td-agent`
  * /var/log/td-agent -> /var/log/fluent
    * 例: `sudo ln -sf /var/log/fluent /var/log/td-agent`
  * /usr/sbin/td-agent -> /usr/sbin/fluentd
    * 例: `sudo ln -sf /usr/sbin/fluentd /usr/sbin/td-agent`
  * /usr/sbin/td-agent-gem -> /usr/sbin/fluent-gem
    * 例: `sudo ln -sf /usr/sbin/fluent-gem /usr/sbin/td-agent-gem`
* 設定ファイルの再配置
  * 退避した設定ファイル群を戻す例: `sudo mv /tmp/td-agent/*.conf /etc/fluent/`
* 旧ログの再配置
  * 退避したログを戻す例: `sudo mv /tmp/td-agent-log/* /var/log/fluent/`
* v3向けに追加でインストールしていたプラグインの再インストール
* サービスを明示的に起動し直す
  * 例: `sudo systemctl start fluentd`

あえてv3からv5に直接更新したいという場合には、設定ファイル群やバッファ等の退避と再配置、シンボリックリンクの作成などケアしないといけない事が増えます。繰り返しになりますが、よほどの事情がない限り直接v3からv5へのアップグレードは実施してはいけません。

## CentOS 7関連のアップグレードに関する注意喚起

CentOS 7のEOLにともない、Fluent Package v5.0.4以降の新しいバージョンに関してCentOS 7向けのLTS版/通常版いずれもパッケージ提供を終了しています。
これはCentOS 7のEOLにともないパッケージのビルド環境を維持できなくなったためです。
したがって、まだCentOS 7の環境でTreasure Agent (td-agent) v3を動かしていた場合、更新できる最新のバージョンはv5.0.3までとなります。


## まとめ

今回は、Treasure Agent (td-agent) v3系から最新のFluent Package v5 (LTS版)へのアップグレード方法と注意点について紹介しました。

クリアコードは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})を行っています。
Fluentdをつかったシステムの設計支援をはじめ、Fluent Packageへのアップデート支援や影響のあるバグ・脆弱性のレポートなどのサポートをします。詳しくは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})をご覧いただき、[お問い合わせフォーム]({% link contact/index.md %})よりお気軽にお問い合わせください。
