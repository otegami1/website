---
tags: []
title: WiX ToolsetでQuiet Execution Custom Actionを使ってバッチ実行時のコマンドプロンプト表示を抑制するには
---
### はじめに

クリアコードでは、[Fluentd](https://www.fluentd.org/)の開発に参加しています。Fluentdにはtd-agentと呼ばれるディストリビューションがあり、各種プラグインをまとめてパッケージやインストーラーが提供されています。
<!--more-->


今回は、td-agentをWindowsでインストールする際、バッチ実行時にコマンドプロンプトの表示を抑制するのに使った方法について紹介します。

なおWiXのバージョンはこの記事を書いている時点の最新版である3.11.2を前提としています。

### コマンドプロンプトの表示を抑制したい

td-agentでは、Windowsでインストールするプロセスの最後に、後始末やFluentdをサービスとして登録する目的でいくつかバッチファイルを実行しています。
その際、コマンドプロンプトが表示されてしまうので、ちょっとインストーラーの出来としてはいまいちでした。

### Quiet Execution Custom Actionを使ってみる

WiXには[Quiet Execution Custom Action](https://wixtoolset.org/documentation/manual/v3/customactions/qtexec.html)というカスタムアクションを定義するためのものがあります。
それを使って、バッチ実行時のコマンドプロンプト表示を抑制するようにしたのが次の.wxsです。

```xml
<?xml version='1.0'?>
<Wix xmlns="http://schemas.microsoft.com/wix/2006/wi" xmlns:util="http://schemas.microsoft.com/wix/UtilExtension">

...(途中省略)..

    <!-- td-agent-post-install.batに関するカスタムアクションを指定する -->
    <Property Id="PostInstall" Value=" "/>
    <CustomAction Id="SetPostInstallCommand"
                  Property="PostInstall"
                  Value="&quot;[PROJECTLOCATION]bin\td-agent-post-install.bat&quot;"/>
    <CustomAction Id="PostInstall"
                  BinaryKey="WixCA"
                  DllEntry="WixQuietExec64"
                  Execute="deferred"
                  Return="check"
                  Impersonate="no" />

    <!-- td-agent.batに関するカスタムアクションを指定する -->
    <Property Id="InstallFluentdWinSvc" Value=" "/>
    <CustomAction Id="SetInstallFluentdWinSvcCommand"
                  Property="InstallFluentdWinSvc"
                  Value="&quot;[PROJECTLOCATION]bin\td-agent.bat&quot; --reg-winsvc i --reg-winsvc-delay-start --reg-winsvc-auto-start --reg-winsvc-fluentdop
t &quot;-c [PROJECTLOCATION]etc\td-agent\td-agent.conf -o [PROJECTLOCATION]td-agent.log&quot;"/>
    <CustomAction Id="InstallFluentdWinSvc"
                  BinaryKey="WixCA"
                  DllEntry="WixQuietExec64"
                  Execute="deferred"
                  Return="check"
                  Impersonate="no" />

    <!-- 以下でインストール時の実行順を指定している -->
    <InstallExecuteSequence>
      <Custom Action="SetPostInstallCommand" After="InstallFiles">NOT Installed</Custom>
      <Custom Action="PostInstall" After="SetPostInstallCommand">NOT Installed</Custom>
      <Custom Action="SetInstallFluentdWinSvcCommand" After="InstallFiles">NOT Installed</Custom>
      <Custom Action="InstallFluentdWinSvc" After="SetInstallFluentdWinSvcCommand">NOT Installed</Custom>
    </InstallExecuteSequence>
 </Product>
</Wix>
```


上記サンプルは、3つの部分から構成されています。

  * td-agent-post-install.batに関するカスタムアクションを指定する

  * td-agent.batに関するカスタムアクションを指定する

  * インストール時のカスタムアクションの実行順序を指定する

#### td-agent-post-install.batに関するカスタムアクションを指定する

```xml
<!-- td-agent-post-install.batに関するカスタムアクションを指定する -->
<Property Id="PostInstall" Value=" "/>
<CustomAction Id="SetPostInstallCommand"
              Property="PostInstall"
              Value="&quot;[PROJECTLOCATION]bin\td-agent-post-install.bat&quot;"/>
<CustomAction Id="PostInstall"
              BinaryKey="WixCA"
              DllEntry="WixQuietExec64"
              Execute="deferred"
              Return="check"
              Impersonate="no" />
```


上記はプロパティ `PostInstall` の値を設定するカスタムアクション `SetPostInstallCommand` と `WinQuietExec64`を使ってコマンドプロンプトを表示せずに実行するための`PostInstall`カスタムアクションから構成されています。

プロパティ `PostInstall` の初期値が `Value=" "` となっているのは意図したものです。このようにしないとエラーになります。
また、実行するコマンドを指定するときには、 `&quot;` で囲ってあげないといけません。

`WixQuietExec64` で実行するコマンドは、カスタムアクションの`Id`と同名の`Property`で指定するというのがポイントです。

なお、`WixQuietExec64` を使えるようにするために、 `light.exe` 実行時に `-ext WixUtilExtension` オプションを明示的に指定して実行する必要があることに注意してください。

#### td-agent.batに関するカスタムアクションを指定する

`td-agent-post-install.bat`に関するカスタムアクションを指定する部分とほぼ同様なので説明を省略します。

#### インストール時のカスタムアクションの実行順序を指定する

```xml
<!-- 以下でインストール時の実行順を指定している -->
<InstallExecuteSequence>
  <Custom Action="SetPostInstallCommand" After="InstallFiles">NOT Installed</Custom>
  <Custom Action="PostInstall" After="SetPostInstallCommand">NOT Installed</Custom>
  <Custom Action="SetInstallFluentdWinSvcCommand" After="InstallFiles">NOT Installed</Custom>
  <Custom Action="InstallFluentdWinSvc" After="SetInstallFluentdWinSvcCommand">NOT Installed</Custom>
</InstallExecuteSequence>
```


上記は、インストール時に実行されるカスタムアクションの実行順序を指定しています。上から順に実行されます。
それぞれのカスタムアクションの意味は次のとおりです。

  * インストール後に実行というのは `After="InstallFiles"`で指定し、カスタムアクション`SetPostInstallCommand`を実行する

  * カスタムアクション`SetPostInstallCommand`の後に`PostInstall`を実行するというのを`After="SetPostInstallCommand"`で指定する

  * インストール後に実行というのは `After="InstallFiles"`で指定し、カスタムアクション`SetFluentdWinSvcCommand`を実行する

  * カスタムアクション`SetFluentdWinSvcCommand`の後に`InstallFluentdWinSvc`を実行するというのを`After="SetFluentdWinSvcCommand"`で指定する

すでに説明した、プロパティの設定と、コマンドを実行するためのカスタムアクションのペアであることがわかります。

### まとめ

今回は、WiX ToolsetでQuiet Execution Custom Actionを使ってバッチ実行時のコマンドプロンプト表示を抑制する方法を紹介しました。
td-agent 4.0.1のリリースには間にあわなかったのですが、次の4.x系のリリースに含まれるはずです。

当社では、お客さまからの技術的なご質問・ご依頼に有償にて対応する[Fluentdサポートサービス](https://www.clear-code.com/services/fluentd.html)を提供しています。Fluentd/Fluent Bitをエンタープライズ環境において導入/運用されるSIer様、サービス提供事業者様は、お問い合わせフォームよりお問い合わせください。
