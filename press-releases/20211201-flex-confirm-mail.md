---
layout: default
main_title: メール誤送信防止アドオン Flex Confirm Mail 4.0.2をリリース
sub_title:
type: press
---

<p class='press-release-subtitle-small'>
企業・組織内での実際の運用から追加された様々なメール誤送信防止機能を簡単に追加・管理できるアドオン</p>

<div class="press-release-signature">
  <p class="date">2021年12月1日</p>
  <p>株式会社クリアコード</p>
</div>



株式会社クリアコード（本社：埼玉県所沢市、代表取締役：須藤 功平)は、2014年2月のリリースから多くの方に利用されてきたメール誤送信防止アドオン「Flex Confirm Mail」をより使いやすく刷新し公開したことをお知らせします。　

「Flex Confirm Mail」は、組織内でのメール運用において、情報漏洩などの観点からセキュリティリスクが高い誤送信を防止するのに有効な、送信先に関する設定や注意喚起の警告などの設定管理することを可能にするメールクライアント「Thunderbird」[^1]向けのアドオンです。クリアコードが2006年から、「Thunderbird」の開発、導入支援及びサポートサービスを提供してきたなかで、様々な企業・組織が実際に運用し、必要となった機能を数多く実装してきています。今回のメジャーバージョンアップでは、誤送信防止のためのさまざまな対策を容易に有効化できるよう、設定UIの改善を図りました。

[^1]: Thunderbirdは、Mozilla Foundationの米国およびその他の国における商標または登録商標です。

<div class="grid-x grid-padding-x">
<figure class="cell medium-6">
  <figcaption>新しい警告条件設定画面</figcaption>
  <a target="_blank" href="{% link press-releases/20211201-flex-confirm-mail/setting-img.png %}">
    <img alt="new-setting" src="{% link press-releases/20211201-flex-confirm-mail/setting-img.png %}">
  </a>
</figure>
<figure class="cell medium-6">
  <figcaption>送信時の確認表示画面</figcaption>
  <a target="_blank" href="{% link press-releases/20211201-flex-confirm-mail/alert-img.png %}">
    <img alt="alert-view" src="{% link press-releases/20211201-flex-confirm-mail/alert-img.png %}">
  </a>
</figure>
</div>


## Flex Confirm Mailについて

### 基本機能
Flex Confirm Mailは、メールを送信する前に、いったん「確認」する段階を挟むことで、不用意なメールの誤送信を防止し、情報漏洩などのセキュリティリスクの軽減をサポートします。
実際の運用のなかで有用に使われている警告として、以下のような条件を柔軟に複数設定することが可能です。

【代表的な警告条件】

1. 警告の例外とするドメインの登録

   特定のドメインを自社やグループ会社のドメインとして登録することにより、それ以外のドメイン宛のアドレスが宛先に含まれる場合にのみ警告します。

2. セキュリティリスクが懸念される操作の後の警告

   宛先欄の手動編集操作や、宛先が異なるメールからの本文のコピー＆ペースト、外部アプリケーションからの本文のコピー＆ペーストなど、特に誤送信が発生しやすい状況に限定して警告します。

3. 危険なドメインの宛先の警告

   なりすましのドッペルゲンガードメインなどを事前に登録しておくことで、メール送信前に警告を表示したり、メール送信を禁止したりできます。

4. 件名や本文に含まれる語句での警告

   特別に注意を要する語句を事前に登録しておくことで、件名や本文にそれらが含まれる場合に、メール送信前に警告を表示したり、メール送信を禁止したりできます。

5. 添付ファイルを条件とした警告

   注意を要するファイル名の語句や拡張子を事前に登録しておくことで、条件に当てはまる添付ファイルを送信しようとした場合に、メール送信前に警告を表示したり、メール送信を禁止したりできます。
   また、添付ファイルを含むメールを特定のドメインに送信した場合を条件としての、警告やメール送信禁止設定も可能です。



### アドオン
[Thunderbird アドオン　Flex Confirm Mail](https://addons.thunderbird.net/ja/thunderbird/addon/flex-confirm-mail/)(外部リンク)

  アドオンの利用には、Thunderbird本体のインストールが必要です。


## Firefox/Thunderbirdサポートについて
2006年の創業以来、ユーザー数50人から8万人までさまざまな規模の50社以上のお客さまに、FirefoxやThunderbirdなどのMozilla製品の技術サポートサービスをご提供しています。

担当エンジニアはMozilla製品の開発コミュニティに参加しており、サポート事業の中で判明した物も含め多数のフィードバックを行ってきた実績があります。確かな知識に基づくサービスにより、他では対応不可能な事例にも対応できる事業者としてご好評を頂いています。

導入時の設定に関するサポートや、必要な機能の追加実装、個別のインストーラーの準備や、12か月に1度の長期サポート版の更新時の調査などお客様のニーズに合わせて柔軟に組み合わせご提案することも可能です。お気軽にお問合せください。


## クリアコードについて
クリアコードは、 2006年7月にフリーソフトウェア開発者を中心に設立したソフトウェア開発会社です。クリアコードの目的は、単に会社を継続していくことではありません。フリーソフトウェアの開発で学んだことを継続的にビジネス分野に活用していくことで会社を継続し、それと同時に、ビジネスを継続することでフリーソフトウェアへ継続的にコミットメントしていくこと、この両立の実現が当社の目的です。この理念は、我々がフリーソフトウェアの開発で学んだことがベースとなっています。



### 参考URL
【コーポレートサイト】[{{ site.url }}{% link index.html %}]({% link index.html %})

【本プレスリリース】[{{ site.url }}{% link press-releases/20211201-flex-confirm-mail.md %}]({% link press-releases/20211201-flex-confirm-mail.md%})

【関連サービス】[{{ site.url }}{% link services/mozilla/menu.html %}]({% link services/mozilla/menu.html%})

### 当リリースに関するお問合せ先
株式会社クリアコード

TEL：04-2907-4726

メール：info@clear-code.com
